package com.pinyougou.pay.service;

import com.pinyougou.pojo.TbPayLog;

import java.util.Map;

public interface PayService {

    /**
     * 生成二维码
     * @out_trade_no : 订单号
     * @total_fee : 支付金额（单位：分）
     */

    public Map<String,Object> createNative(String out_trade_no, String total_fee) throws Exception;


    public Map<String,String> queryPayStatus(String out_trade_no) throws Exception;

    TbPayLog getPayLog(String userId);

    void updatePayStatus(String out_trade_no, String transaction_id);

    void judgeTimeOut(String userId, String out_trade_no, long time);

    Long getPayLogMoneyByOutTradeNo(String out_trade_no);
}
