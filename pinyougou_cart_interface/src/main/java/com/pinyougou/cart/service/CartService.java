package com.pinyougou.cart.service;

import com.pinyougou.pojo.TbItem;
import groupEntity.Cart;

import java.util.List;
import java.util.Map;

public interface CartService {

    //添加商品到购物车
    Map<String,Object> addItemToCartList(List<Cart> cartList, Long itemId, Integer num);

    void saveCartListByKey(String key, List<Cart> cartList);

    List<Cart> selectCartListByKey(String key);

    void saveCartListByUsername(String username, List<Cart> cartList);

    List<Cart> mergeCartList(List<Cart> cartList_sessionId, List<Cart> cartList_username);

    void deleCartListBySessionId(String sessionId);

    List<TbItem> selectTitleByItemId(Long itemId);

    List<Cart> changeCartList(List<Cart> cartList, Long newItemId, Long oldItemId);
}
