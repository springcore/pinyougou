package com.pinyougou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.*;
import com.pinyougou.pojo.*;
import com.pinyougou.pojo.TbGoodsExample.Criteria;
import com.pinyougou.sellergoods.service.GoodsService;
import entity.PageResult;
import groupEntity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 服务实现层
 * @author Administrator
 *
 */
@Service
public class GoodsServiceImpl implements GoodsService {

	@Autowired
	private TbGoodsMapper goodsMapper;
	
	/**
	 * 查询全部
	 */
	@Override
	public List<TbGoods> findAll() {
		return goodsMapper.selectByExample(null);
	}

	/**
	 * 按分页查询
	 */
	@Override
	public PageResult findPage(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);		
		Page<TbGoods> page=   (Page<TbGoods>) goodsMapper.selectByExample(null);
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Autowired
	private TbGoodsDescMapper goodsDescMapper;

	@Autowired
	private TbItemCatMapper itemCatMapper;

	@Autowired
	private TbBrandMapper brandMapper;

	@Autowired
	private TbSellerMapper sellerMapper;

	@Autowired
	private TbItemMapper itemMapper;

	/**
	 * 增加
	 */
	@Override
	public void add(Goods goods) {
		//tb_goods表数据保存
		TbGoods tbGoods = goods.getGoods();
		tbGoods.setAuditStatus("0");//未审核状态
		goodsMapper.insert(tbGoods);

		//tb_goods_desc表数据保存
		TbGoodsDesc goodsDesc = goods.getGoodsDesc();
		goodsDesc.setGoodsId(tbGoods.getId());
		goodsDescMapper.insert(goodsDesc);

		if(tbGoods.getIsEnableSpec().equals("1")){//启用规格
			//tb_item表数据保存
			List<TbItem> itemList = goods.getItemList();
			for (TbItem item : itemList) {
//		  `title` varchar(100) NOT NULL COMMENT '商品标题',   // 商品名称（SPU名称）+ 商品规格选项名称 中间以空格隔开
				String title=tbGoods.getGoodsName();
				//获取商品规格选项名称
				//:{"网络":"移动3G","机身内存":"32G"}
				String spec = item.getSpec();
				Map<String,String> map = JSON.parseObject(spec, Map.class);
				for(String key : map.keySet()){
					title+=" "+ map.get(key);
				}
				item.setTitle(title);
				//封装tb_item后端需要组装的数据
				setItemValue(tbGoods, goodsDesc, item);
				itemMapper.insert(item);
			}
		}else {//没有启用规格
			//需要后端自己组装sku（item）的相关数据
			TbItem item = new TbItem();
			//spu名称作为title
			String title=tbGoods.getGoodsName();
			item.setTitle(title);

			//封装tb_item后端需要组装的数据
			setItemValue(tbGoods, goodsDesc, item);

			//封装原本由页面提交的数据
//			`spec` varchar(200) DEFAULT NULL,
			item.setSpec("{}");
//		 `price` decimal(20,2) NOT NULL COMMENT '商品价格，单位为：元',
			item.setPrice(tbGoods.getPrice());
//		 `num` int(10) NOT NULL COMMENT '库存数量',
			item.setNum(999);
//		 `status` varchar(1) NOT NULL COMMENT '商品状态，1-正常，2-下架，3-删除',
			item.setStatus("1");
//		 `is_default` varchar(1) DEFAULT NULL,
			item.setIsDefault("1");

			itemMapper.insert(item);
		}


	}

	/**
	 * 封装tb_item后端需要组装的数据
	 * @param tbGoods
	 * @param goodsDesc
	 * @param item
	 */
	private void setItemValue(TbGoods tbGoods, TbGoodsDesc goodsDesc, TbItem item) {
		//		  `image` varchar(2000) DEFAULT NULL COMMENT '商品图片',  // 从 tb_goods_desc item_images中获取第一张
		//[{"color":"白色","url":"http://192.168.25.133/group1/M00/00/00/wKgZhVnGbYuAO6AHAAjlKdWCzvg253.jpg"},
		// {"color":"黑色","url":"http://192.168.25.133/group1/M00/00/00/wKgZhVnKX5WAOsqXAAETwD7A1Is409.jpg"}]
		String itemImages = goodsDesc.getItemImages();
		List<Map> imageList = JSON.parseArray(itemImages, Map.class);
		if (imageList!=null && imageList.size()>0) {
            String image= (String) imageList.get(0).get("url");
            item.setImage(image);
        }
//		  `categoryId` bigint(10) NOT NULL COMMENT '所属类目，叶子类目',  //三级分类id
		item.setCategoryid(tbGoods.getCategory3Id());
//		  `create_time` datetime NOT NULL COMMENT '创建时间',
		item.setCreateTime(new Date());
//		  `update_time` datetime NOT NULL COMMENT '更新时间',
		item.setUpdateTime(new Date());
//		  `goods_id` bigint(20) DEFAULT NULL,
		item.setGoodsId(tbGoods.getId());
//		  `seller_id` varchar(30) DEFAULT NULL,
		item.setSellerId(tbGoods.getSellerId());
//				//以下字段作用：方便商品搜索
//		  `category` varchar(200) DEFAULT NULL, //三级分类名称
		String category = itemCatMapper.selectByPrimaryKey(tbGoods.getCategory3Id()).getName();
		item.setCategory(category);
//		  `brand` varchar(100) DEFAULT NULL,//品牌名称
		String brandName = brandMapper.selectByPrimaryKey(tbGoods.getBrandId()).getName();
		item.setBrand(brandName);
//		  `seller` varchar(200) DEFAULT NULL,//商家店铺名称
		String nickName = sellerMapper.selectByPrimaryKey(tbGoods.getSellerId()).getNickName();
		item.setSeller(nickName);
	}


	/**
	 * 修改
	 */
	@Override
	public void update(Goods goods){
		//修改商品需要修改商品的状态为未审核状态
		TbGoods goods1 = goods.getGoods();
		goods1.setAuditStatus("0");
		//修改商品
		goodsMapper.updateByPrimaryKey(goods1);
		//修改规格选项
		goodsDescMapper.updateByPrimaryKey(goods.getGoodsDesc());
		//修该sku列表选项
		//修改前选删除对应的sku列表数据
		//sku表中维护着商品的id，通过商品的id删除对应sku数据
		TbItemExample example=new TbItemExample();
		example.createCriteria().andGoodsIdEqualTo(goods1.getId());
		itemMapper.deleteByExample(example);
		//调用抽取的私有方法保存修改后的item数据
		//3、实现对items表中数据的保存
		List<TbItem> itemList = goods.getItemList();
		for (TbItem item : itemList) {
			//标题 spu名称+商品规格选项名称；空格隔开
			//spu的名称
			String title = goods.getGoods().getGoodsName();
			//获取商品规格选项的名称
			Map<String, String> specMap = JSON.parseObject(item.getSpec(), Map.class);
			for (String key : specMap.keySet()) {
				title += " " + specMap.get(key);
			}
			item.setTitle(title);
			//后台组装的数据
			setItemValue(goods1,goods.getGoodsDesc(),item);
			itemMapper.insert(item);
		}
	}	
	
	/**
	 * 根据ID获取实体;数据的回显
	 * @param id
	 * @return
	 */
	@Override
	public Goods findOne(Long id){
		//用于组装数据
		 Goods goods = new Goods();
		 //查询对应的商品信息
		 TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
		 goods.setGoods(tbGoods);
		 //查询相关的商品描述信息
		 TbGoodsDesc goodsDesc = goodsDescMapper.selectByPrimaryKey(id);
		 goods.setGoodsDesc(goodsDesc);
		//查询相关的sku数据
		//查询sku数据列表
		TbItemExample example=new TbItemExample();
		example.createCriteria().andGoodsIdEqualTo(id);
		List<TbItem> itemList = itemMapper.selectByExample(example);
		goods.setItemList(itemList);
		return goods;
	}

	/**
	 * 批量删除
	 */
	@Override
	public void delete(Long[] ids) {
		for(Long id:ids){
			goodsMapper.deleteByPrimaryKey(id);
		}		
	}
	
	
		@Override
	public PageResult findPage(TbGoods goods, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		
		TbGoodsExample example=new TbGoodsExample();
		Criteria criteria = example.createCriteria();
		
		if(goods!=null){			
			if(goods.getSellerId()!=null && goods.getSellerId().length()>0){
				//criteria.andSellerIdLike("%"+goods.getSellerId()+"%"); alibaba
				criteria.andSellerIdEqualTo(goods.getSellerId());
			}
			if(goods.getGoodsName()!=null && goods.getGoodsName().length()>0){
				criteria.andGoodsNameLike("%"+goods.getGoodsName()+"%");
			}
			if(goods.getAuditStatus()!=null && goods.getAuditStatus().length()>0){
				//criteria.andAuditStatusLike("%"+goods.getAuditStatus()+"%");
				criteria.andAuditStatusEqualTo(goods.getAuditStatus());
			}
			if(goods.getIsMarketable()!=null && goods.getIsMarketable().length()>0){
				criteria.andIsMarketableLike("%"+goods.getIsMarketable()+"%");
			}
			if(goods.getCaption()!=null && goods.getCaption().length()>0){
				criteria.andCaptionLike("%"+goods.getCaption()+"%");
			}
			if(goods.getSmallPic()!=null && goods.getSmallPic().length()>0){
				criteria.andSmallPicLike("%"+goods.getSmallPic()+"%");
			}
			if(goods.getIsEnableSpec()!=null && goods.getIsEnableSpec().length()>0){
				criteria.andIsEnableSpecLike("%"+goods.getIsEnableSpec()+"%");
			}
			if(goods.getIsDelete()!=null && goods.getIsDelete().length()>0){
				criteria.andIsDeleteLike("%"+goods.getIsDelete()+"%");
			}
	
		}
		
		Page<TbGoods> page= (Page<TbGoods>)goodsMapper.selectByExample(example);		
		return new PageResult(page.getTotal(), page.getResult());
	}

	@Override
	public void updateStatus(Long[] ids, String status) {
		//商品审核
		for (Long id : ids) {
			TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);
			tbGoods.setAuditStatus(status);
			goodsMapper.updateByPrimaryKey(tbGoods);
		}
	}

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private Destination addItemSolrDestination;

	@Autowired
	private Destination deleItemSolrDestination;

	@Autowired
	private Destination addItemPageDestination;

	@Autowired
	private Destination deleItemPageDestination;


	@Override
	public void updateIsMarketable(Long[] ids, String isMarketable) {
		for (Long id : ids) {
			TbGoods tbGoods = goodsMapper.selectByPrimaryKey(id);

			//只有审核通过的商品才能上下架
			if (tbGoods.getAuditStatus().equals("1")) {
				tbGoods.setIsMarketable(isMarketable);
				goodsMapper.updateByPrimaryKey(tbGoods);

				//商品上架，同步上架商品到索引库消息
				if("1".equals(isMarketable)){
					jmsTemplate.send(addItemSolrDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(id+"");
						}
					});
				}

				//商品上架，同步生成上架商品静态页
				if("1".equals(isMarketable)){
					jmsTemplate.send(addItemPageDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(id+"");
						}
					});
				}


				//商品下架，同步删除索引库中下架商品
				if("0".equals(isMarketable)){
					jmsTemplate.send(deleItemSolrDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(id+"");
						}
					});
				}

				//商品下架，同步删除下架商品静态页
				if("0".equals(isMarketable)){
					jmsTemplate.send(deleItemPageDestination, new MessageCreator() {
						@Override
						public Message createMessage(Session session) throws JMSException {
							return session.createTextMessage(id+"");
						}
					});
				}

			}else {
				throw new RuntimeException("只有审核通过的商品才能上下架");
			}
		}
	}

	/**
	 * 查询相关的charpie数据
	 * @return
	 */
	@Override
	public List<Map<String, Object>> charNumber() {
		return goodsMapper.charNumber();
	}

	/**
	 * 查询;charbar数据
	 * @return
	 */
	@Override
	public List<Map<String, Object>> charBarNum() {
		return goodsMapper.charBarNum();
	}

	/**
	 * 查询charLine的相关数据
	 * @return
	 */
	@Override
	public List<Map<String, Object>> charlineNum() {
		return goodsMapper.charlineNum();
	}
}
