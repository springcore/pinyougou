package com.pinyougou.user.service;

import com.pinyougou.pojo.TbSeller;
import com.pinyougou.sellergoods.service.SellerService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserDetailServiceImpl implements UserDetailsService{

    private SellerService sellerService;

    public void setSellerService(SellerService sellerService) {
        this.sellerService = sellerService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //认证服务类，做认证和授权操作

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_SELLER"));

        //基于商家名称，查询商家信息
        TbSeller seller = sellerService.findOne(username);

        if (seller!=null) {
            //只有审核通过的商家才能登陆成功
            if (seller.getStatus().equals("1")) {
                //参数一：用户名 参数二：用户密码 参数三：用户具有的角色和权限集合
                return new User(username,seller.getPassword(),authorities);
            }else {
                return null;
            }
        }else {
            return null;
        }


    }
}
