 //控制层 
app.controller('goodsController' ,function($scope,$controller ,$location  ,goodsService,itemCatService,typeTemplateService,uploadService){
	
	$controller('baseController',{$scope:$scope});//继承
	
    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
		goodsService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){			
		goodsService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(){
		//获取路由传参传递的id
		var id=$location.search()['id'];
		if(id==null){
			return;
		}
		goodsService.findOne(id).success(
			function(response){
				$scope.entity= response;
                //向富文本编辑器中添加数据(选从对象中取出来)
                editor.html($scope.entity.goodsDesc.introduction);
                //回显照片
                //[{"color":"红色","url":"http://192.168.25.133/group1/M00/00/01/wKgZhVmHINKADo__AAjlKdWCzvg874.jpg"},{"color":"黑色","url":"http://192.168.25.133/group1/M00/00/01/wKgZhVmHINyAQAXHAAgawLS1G5Y136.jpg"}]
                //将返回的图片转换为json对象
                $scope.entity.goodsDesc.itemImages=JSON.parse(
                    $scope.entity.goodsDesc.itemImages);
                //回显商品的自定义属性
                $scope.entity.goodsDesc.customAttributeItems=JSON.parse(
                    $scope.entity.goodsDesc.customAttributeItems);

                //回显规格数据
                $scope.entity.goodsDesc.specificationItems=JSON.parse(
                    $scope.entity.goodsDesc.specificationItems);
                //处理返回的sku表的数据
                for(var i=0;i<$scope.entity.itemList.length<0;i++){
                    $scope.entity.itemList[i].spec=JSON.parse($scope.entity.itemList[i].spec);
                }
			}
		);				
	}
	//根据规格名称，对应的规格选项的值，判断是否选中
	$scope.checkSpec=function (specValue, optionValue) {
		//通过baseController中提取的方法判断是否选中
        //[{"attributeName":"网络制式","attributeValue":["移动3G","移动4G"]},{"attributeName":"屏幕尺寸","attributeValue":["6寸","5.5寸"]}]
        var object= $scope.getObjectByValue($scope.entity.goodsDesc.specificationItems,'attributeName',specValue);
        if(object==null){
        	return false;
		}else {
        	if (object.attributeValue.indexOf(optionValue)!=-1){
					return true;
			}else {
        		return false;
			}
		}
    }

	//保存 
	$scope.save=function(){
        //绑定kindEditor编辑器中商品介绍html片段
        $scope.entity.goodsDesc.introduction= editor.html();
		var serviceObject;//服务层对象  				
		if($scope.entity.goods.id!=null){//如果有ID
			serviceObject=goodsService.update( $scope.entity ); //修改

		}else{

			serviceObject=goodsService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
		        	$scope.entity={};//临时方案，清空输入框的内容
					//修改添加成功跳转到上查询表页面
					location.href="goods.html";
					//清空kindEditor编辑器中内容
                    editor.html("");
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		goodsService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		goodsService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}

	//查询一级分类列表数据
	$scope.selectItemCat1List=function () {
		itemCatService.findByParentId(0).success(function (response) {
			$scope.itemCat1List=response;
           // $scope.itemCat3List=[];
        })
    }

    //基于一级分类的改变，联动查询二级分类列表数据
	//参数一：监控发生变化的模型数据 参数二：当监控的数据发生变化后，要处理的事情
	//newValue 变化后的模型数据值  oldValue 变化前的模型数据值
    $scope.$watch("entity.goods.category1Id",function (newValue,oldValue) {
        itemCatService.findByParentId(newValue).success(function (response) {
            $scope.itemCat2List=response;
        })
    });

    //基于二级分类的改变，联动查询三级分类列表数据
    //参数一：监控发生变化的模型数据 参数二：当监控的数据发生变化后，要处理的事情
    //newValue 变化后的模型数据值  oldValue 变化前的模型数据值
    $scope.$watch("entity.goods.category2Id",function (newValue,oldValue) {
        itemCatService.findByParentId(newValue).success(function (response) {
            $scope.itemCat3List=response;
        })
    });

    //基于三级分类的改变，联动查询模板数据
    //参数一：监控发生变化的模型数据 参数二：当监控的数据发生变化后，要处理的事情
    //newValue 变化后的模型数据值  oldValue 变化前的模型数据值
    $scope.$watch("entity.goods.category3Id",function (newValue,oldValue) {
        itemCatService.findOne(newValue).success(function (response) {
            $scope.entity.goods.typeTemplateId=response.typeId;
        })
    });

    //基于模板id的改变，联动查询模板关联的品牌列表数据
    //参数一：监控发生变化的模型数据 参数二：当监控的数据发生变化后，要处理的事情
    //newValue 变化后的模型数据值  oldValue 变化前的模型数据值
    $scope.$watch("entity.goods.typeTemplateId",function (newValue,oldValue) {
        typeTemplateService.findOne(newValue).success(function (response) {
            //展示品牌列表
			$scope.brandList=JSON.parse(response.brandIds);
            //展示扩展属性列表  response.customAttributeItems = [{"text":"内存大小"},{"text":"颜色"}]
			if($location.search()['id']==null) {
                $scope.entity.goodsDesc.customAttributeItems = JSON.parse(response.customAttributeItems);
            }
        });
        //查询模板关联的规格列表
        typeTemplateService.selectSpecList(newValue).success(function (response) {
			$scope.specList=response;
        })
    });

    //初始化图片对象
	$scope.imageEntity={};

    //文件上传功能
	$scope.uploadFile=function () {
		uploadService.uploadFile().success(function (response) {
			if(response.success){
				//上传成功，图片回显
				$scope.imageEntity.url=response.message;
			}else {
				alert(response.message);
			}
        })
    }

    //初始化entity对象
	$scope.entity={goods:{isEnableSpec:'1'},goodsDesc:{itemImages:[],specificationItems:[]},itemList:[]};

    //完成图片保存功能
	$scope.saveImage=function () {
        $scope.entity.goodsDesc.itemImages.push($scope.imageEntity);
    }

    //完成删除图片功能
    $scope.deleImage=function (index) {
        $scope.entity.goodsDesc.itemImages.splice(index,1);
    }
    
    //规格选项勾选和取消勾选功能
	$scope.updateSpecAttribute=function ($event,specName,specOption) {
		//判断规格名称对应的规格对象是否存在与勾选的规格列表中,如果存在，则返回该对象
		//[{"attributeName":"网络","attributeValue":["移动3G"]}]
        var specObject = $scope.getObjectByValue($scope.entity.goodsDesc.specificationItems,"attributeName",specName);

        if(specObject!=null){//如果存在
			//判断是勾选还是取消勾选规格选择数据
			if($event.target.checked){//勾选
                //在已存在的规格对象中规格选项列表中添加勾选的规格选项数据
                specObject.attributeValue.push(specOption);
			}else {//取消勾选规格选择
				//在已存在的规格对象中规格选项列表中移除取消勾选的规格选项数据
                specObject.attributeValue.splice( specObject.attributeValue.indexOf(specOption),1);

                //如果规格对象中规格选项列表中的规格选项数据全部移除
				if(specObject.attributeValue.length==0){
					//从勾选的规格列表中，移除该规格对象
					var index=$scope.entity.goodsDesc.specificationItems.indexOf(specObject);
                    $scope.entity.goodsDesc.specificationItems.splice(index,1);
				}

			}

		}else {//如果不存在
			//新增规格对象到勾选的规格列表中
            $scope.entity.goodsDesc.specificationItems.push({"attributeName":specName,"attributeValue":[specOption]});
		}
    }

    //构建组装item列表
	$scope.createItemList=function () {
		// spec:{"机身内存":"16G","网络":"联通3G"}
		//初始化itemList列表
        $scope.entity.itemList=[{spec:{},price:0.00,num:999,status:"1",isDefault:"0"}];
		//
		//勾选的规格结果集数组 [{"attributeName":"网络","attributeValue":["移动3G"]}]
        var checkSpecList= $scope.entity.goodsDesc.specificationItems;
        //全部取消勾选的规格结果集
		if(checkSpecList.length==0){
            $scope.entity.itemList=[];
		}

		//获取勾选的规格名称和规格选项列表
		for(var i=0;i<checkSpecList.length;i++){
			//抽取方法，动态的组装itemList中对象的spec属性赋值过程
            $scope.entity.itemList = addColumn($scope.entity.itemList,checkSpecList[i].attributeName,checkSpecList[i].attributeValue);
		}
    }


	//动态的组装itemList中对象的spec属性
    addColumn=function (list,specName,specOptions) {
		var newList=[];

		for(var i=0;i<list.length;i++){
			//{spec:{},price:0.00,num:999,status:"1",isDefault:"0"}
			var olditem = list[i];

			//循环遍历规格选项列表，获取规格选项值
			for(var j=0;j<specOptions.length;j++){
				//基于深克隆，构建新的item对象
				var newItem = JSON.parse(JSON.stringify(olditem));

                newItem.spec[specName]=specOptions[j];

                newList.push(newItem);
			}
		}

		return newList;
    }


	//商品审核状态数组
	$scope.status=['未审核','已审核','审核未通过','关闭'];

    //商品上下架数组
    $scope.isMarketable=['下架','上架'];

    //商品上下架
    $scope.updateIsMarketable=function (isMarketable) {
        //获取选中的复选框
        goodsService.updateIsMarketable( $scope.selectIds,isMarketable ).success(
            function(response){
                if(response.success){
                    $scope.reloadList();//刷新列表
                    $scope.selectIds=[];//清空记录审核状态id的数组内容
                }else {
                    alert(response.message);
                }
            }
        );
    }





    
});	
