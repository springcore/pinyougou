package com.pinyougou.search.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.data.solr.core.query.result.HighlightEntry;
import org.springframework.data.solr.core.query.result.HighlightPage;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SearchServiceImpl implements SearchService {

    @Autowired
    private SolrTemplate solrTemplate;

    @Override
    public Map<String, Object> search(Map searchMap) {
        //构建高亮查询对象
        HighlightQuery query = new SimpleHighlightQuery();

        //1、获取页面输入的关键字条件
        String keywords = (String) searchMap.get("keywords");

        //构建关键字搜索条件
        Criteria criteria = null;
        if(keywords!=null && !"".equals(keywords)){
            //输入了关键字
            criteria = new Criteria("item_keywords").is(keywords);
        }else {
            //未输入关键字 ，搜索所有  *:*
            criteria = new Criteria().expression("*:*");
        }

        //将关键字搜索条件赋予查询对象
        query.addCriteria(criteria);

        //2、品牌过滤条件查询
        String brand = (String) searchMap.get("brand");
        if(brand!=null && !"".equals(brand)){
            //设置品牌筛选条件
            Criteria brandCriteria = new Criteria("item_brand").is(brand);
            FilterQuery filterQuery = new SimpleFilterQuery(brandCriteria);
            //将过滤条件查询对象关联到主查询对象
            query.addFilterQuery(filterQuery);
        }

        //3、分类过滤条件查询
        String category = (String) searchMap.get("category");
        if(category!=null && !"".equals(category)){
            //设置分类筛选条件
            Criteria categoryCriteria = new Criteria("item_category").is(category);
            FilterQuery filterQuery = new SimpleFilterQuery(categoryCriteria);
            //将过滤条件查询对象关联到主查询对象
            query.addFilterQuery(filterQuery);
        }

        //4、规格过滤条件查询
        Map<String,String> specMap = (Map<String, String>)searchMap.get("spec");
        if(specMap!=null){
            //从map中获取规格名称和选择的规格选择值
            for(String key : specMap.keySet()){
                //设置规格筛选条件
                Criteria specCriteria = new Criteria("item_spec_"+key).is(specMap.get(key));
                FilterQuery filterQuery = new SimpleFilterQuery(specCriteria);
                //将过滤条件查询对象关联到主查询对象
                query.addFilterQuery(filterQuery);
            }
        }

        //5、价格区间过滤条件查询
        String price = (String) searchMap.get("price");
        if(price!=null && !"".equals(price)){
            //设置价格区间筛选条件 0-1000 1000-2000 2000-*   关键点：临界值判断 最低值：0  最高：*
            String[] prices = price.split("-");

            if(!prices[0].equals("0")){
                //设置价格区间筛选条件
                Criteria priceCriteria = new Criteria("item_price").greaterThanEqual(prices[0]);
                FilterQuery filterQuery = new SimpleFilterQuery(priceCriteria);
                //将过滤条件查询对象关联到主查询对象
                query.addFilterQuery(filterQuery);
            }

            if(!prices[1].equals("*")){
                //设置价格区间筛选条件
                Criteria priceCriteria = new Criteria("item_price").lessThanEqual(prices[1]);
                FilterQuery filterQuery = new SimpleFilterQuery(priceCriteria);
                //将过滤条件查询对象关联到主查询对象
                query.addFilterQuery(filterQuery);
            }

        }



        //6、排序查询
        String sortField = (String) searchMap.get("sortField");
        String sort = (String) searchMap.get("sort");
        if(sortField!=null && !"".equals(sortField)){
            //设置排序条件
            if (sort.equals("ASC")) {//升序
                query.addSort(new Sort(Sort.Direction.ASC,"item_"+sortField));
            }else {
                //降序
                query.addSort(new Sort(Sort.Direction.DESC,"item_"+sortField));
            }
        }

        //7分页条件查询
        Integer pageNo = (Integer) searchMap.get("pageNo");
        Integer pageSize = (Integer) searchMap.get("pageSize");

        query.setOffset((pageNo-1)*pageSize);//分页起始值  (pageNo-1)*pageSise
        query.setRows(pageSize);//每页记录数


        //设置高亮处理代码
        HighlightOptions highlightOptions = new HighlightOptions();//设置高亮对象
        highlightOptions.addField("item_title");//设置高亮字段
        highlightOptions.setSimplePrefix("<font color='red'>");//设置高亮前缀
        highlightOptions.setSimplePostfix("</font>");
        query.setHighlightOptions(highlightOptions);

        //高亮条件分页查询
        HighlightPage<TbItem> page = solrTemplate.queryForHighlightPage(query, TbItem.class);

        //获取当前页商品列表数据
        List<TbItem> content = page.getContent();

        //高亮结果处理
        for (TbItem item : content) {
            //获取高亮内容
            List<HighlightEntry.Highlight> highlights = page.getHighlights(item);
            //判断有没有高亮内容
            if(highlights.size()>0){
                HighlightEntry.Highlight highlight = highlights.get(0);
                //获取高亮内容
                List<String> snipplets = highlight.getSnipplets();
                if(snipplets.size()>0){
                    //替换高亮内容
                    item.setTitle(snipplets.get(0));
                }

            }

        }


        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("rows",content);
        resultMap.put("totalPages",page.getTotalPages());
        resultMap.put("pageNo",pageNo);

        return resultMap;
    }
}
