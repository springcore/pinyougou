package com.pinyougou.search.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.SolrDataQuery;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

public class DeleItemSolrMessageListener implements MessageListener{

    @Autowired
    private SolrTemplate solrTemplate;

    @Override
    public void onMessage(Message message) {
        // 商品下架，同步删除索引库中下架商品
        try {
            TextMessage textMessage = (TextMessage)message;
            //下架消息是商品id值
            String goodsId = textMessage.getText();

            //商品下架，同步删除索引库中下架商品
            SolrDataQuery query = new SimpleQuery("item_goodsid:"+goodsId);
            solrTemplate.delete(query);
            solrTemplate.commit();

        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
