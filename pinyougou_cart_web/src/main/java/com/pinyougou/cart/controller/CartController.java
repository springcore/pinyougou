package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbOrderItem;
import entity.Result;
import groupEntity.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import util.CookieUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/cart")
public class CartController {


    @Reference(timeout = 50000)
    private CartService cartService;

    @RequestMapping("/addItemToCartList")
    //允许来自http://item.pinyougou.com地址的跨域请求访问到当前方法 并允许携带cookie信息（默认允许）
    @CrossOrigin(origins = "http://item.pinyougou.com",allowCredentials = "true")
    public Result addItemToCartList(Long itemId, Integer num){
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            //获取sessionId
            String sessionId = getSessionId();
            //1.从redis中获取购物车列表
            //如果未登录 则基于sessionId到redis中查询
            /**
             * 由于查询购物车列表数据时，前半段是redis中购物车数据，后半段会拼接一段从【数据库查询】的信息，截取掉即可
             */
            List<Cart> cartList = findCartList();
            cartList = cartList.subList(0, cartList.size()/2);
            //2.添加商品到购物车
            Map<String, Object> map = cartService.addItemToCartList(cartList, itemId, num);
            cartList = (List<Cart>) map.get("cartList");
            if ("anonymousUser".equals(username)){//如果是未登录状态
                //3.添加商品后 将列表存入redis更新缓存中的数据
                //如果未登录 基于sessionId取购物车中的信息
                cartService.saveCartListByKey(sessionId,cartList);
            }else {//如果登录了
                //登录后 基于用户名作为key 将购物车列表存入redis
                cartService.saveCartListByUsername(username,cartList);
            }
            if (map.get("status").equals("0")){//如果不需要刷新selectIds
                return new Result(true, "添加商品到购物车成功！");
            }else{//如果需要刷新selectIds
                return new Result(true, "updateSelectIds");
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "添加商品到购物车失败！");
        }
    }


    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;
    @Autowired
    private HttpSession session;

    //基于系统cookie记录sessionId一周时间
    private String getSessionId() {
        //根据页面的request信息 从系统中获取sessionId,如果失效 则返回值为null
        String sessionId = CookieUtil.getCookieValue(request, "cartCookie");
        if (sessionId==null){//如果系统中的cookie失效了
            sessionId = session.getId();
            //将本次从页面获取的sessionId存入系统cookie中
            CookieUtil.setCookie(request,response,"cartCookie",sessionId,3600*24*7, "utf-8");
        }
        return sessionId;
    }


    /**
     * 返回【前半段redis】数据，【后半段数据库】数据
     */
    @RequestMapping("/findCartList")
    //获取购物车列表list=[【redis中数据】,【数据库中数据】]
    //允许来自http://localhost:9007地址的跨域请求访问到当前方法 并允许携带cookie信息（默认允许）
    @CrossOrigin(origins = "http://localhost:9007",allowCredentials = "true")
    public List<Cart> findCartList(){
        //获取登录的用户名
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        String sessionId = getSessionId();
        List<Cart> cartList_sessionId = cartService.selectCartListByKey(sessionId);
        if ("anonymousUser".equals(username)){//如果是未登录状态
            //如果未登录 基于sessionId取购物车中的信息
            return cartList_sessionId;
        }else {//如果登录了
            //登录后 基于用户名作为key 将购物车列表存入redis
            List<Cart> cartList_username = cartService.selectCartListByKey(username);
            if (cartList_sessionId.size()>0){//如果登陆前购物车中有商品 则合并
                cartList_username = cartService.mergeCartList(cartList_sessionId,cartList_username);
                //清除登陆前的购物车列表
                cartService.deleCartListBySessionId(sessionId);
                //合并的结果存入到redis中(只存前半段)
                cartService.saveCartListByUsername(username, cartList_username.subList(0, cartList_username.size()/2));
            }
            return cartList_username;//返回【前半段redis】数据，【后半段数据库】数据
        }
    }

    /**
     * 根据sku的id（itemId）查询所有该spu的所有sku
     * @param itemId
     * @return
     */
    @RequestMapping("/selectTitleByItemId")
    public List<TbItem> selectTitleByItemId(Long itemId){
        return cartService.selectTitleByItemId(itemId);
    }



    /**
     * 改变购物车中商品的规格，将oldItemId的商品更换为newItemId的商品
     * @return
     */
    @RequestMapping("/changeCartList")
    public Result changeCartList(Long newItemId,Long oldItemId){
        try {
            List<Cart> cartList = findCartList();
            //使用列表的前半部分【redis数据】，因为修改购物车中商品规格后，其他商品不能随之改变为数据库数据
            List<Cart> cartList_pre = cartList.subList(0, cartList.size() / 2);
            cartList_pre = cartService.changeCartList(cartList_pre,newItemId,oldItemId);//将商品更换之后，将购物车信息保存至redis
            //TODO 将cartList_pre（购物车数据）存入redis中（已完成✔）
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            String sessionId = getSessionId();
            if ("anonymousUser".equals(username)){//如果是未登录状态
                cartService.saveCartListByKey(sessionId, cartList_pre);//根据sessionId存入redis中
            }else {
                cartService.saveCartListByUsername(username, cartList_pre);//根据username存入redis中
            }
            return new Result(true, "更改购物车商品规格成功！");
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "更改购物车商品规格失败！");
        }
    }
}
