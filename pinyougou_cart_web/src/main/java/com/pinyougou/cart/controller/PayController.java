package com.pinyougou.cart.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pay.service.PayService;
import com.pinyougou.pojo.TbPayLog;
import entity.Result;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/pay")
public class PayController {

    @Reference(timeout = 5000)
    private PayService payService;

    /**
     * 【生成二维码】
     */
    @RequestMapping("/createNative")
    public Map<String, Object> createNative() {
        //TODO 从数据库中查询出订单号以及订单金额信息（已完成✔）
        try {
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            //根据userId获取用户当前订单的支付日志信息
            TbPayLog payLog = payService.getPayLog(userId);
            //参数1：支付订单号，参数2：支付金额
            return payService.createNative(payLog.getOutTradeNo(), payLog.getTotalFee() + "");
        } catch (Exception e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    /**
     * 查询微信支付状态
     */
    @RequestMapping("/queryPayStatus")
    public Result queryPayStatus(String out_trade_no) {
        try {//flag返回true时，表示不需要跳转到支付失败页面，反之 则需要
            String userId = SecurityContextHolder.getContext().getAuthentication().getName();
            //调用service层 判断是否超过订单支付时间，专门用来抛异常用
            payService.judgeTimeOut(userId, out_trade_no, 300000L);//判断总时间是否在规定时间之内(5分钟)
            //调用service层，查询支付状态
            Map<String, String> resultMap = payService.queryPayStatus(out_trade_no);
            String trade_status = resultMap.get("trade_state");
            if ("SUCCESS".equals(trade_status)) {//如果支付成功 则返回
                String transaction_id = resultMap.get("transaction_id");//从查询返回的map中获取微信支付的交易流水号
                payService.updatePayStatus(out_trade_no, transaction_id);//根据支付订单号更新【订单】以及【订单日志】的支付状态
                return new Result(true, "SUCCESS");
            } else {
                return new Result(true, "waitingForPayment");//如果还没有支付，则返回正在等待支付中
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            if ("payTimeOut".equals(e.getMessage())) {//如果是超时
                return new Result(true, "payTimeOut");//返回超时信息
            }
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, "支付时出现了意想之外的错误！");
        }
    }

    /**
     * 根据订单日志id查询订单日志的总支付金额（单位：分）
     */
    @RequestMapping("/getPayLogMoneyByOutTradeNo")
    public Long getPayLogMoneyByOutTradeNo(String out_trade_no) {
        return payService.getPayLogMoneyByOutTradeNo(out_trade_no);
    }


}
