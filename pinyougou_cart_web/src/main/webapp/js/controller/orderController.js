//控制层
module.controller('orderController', function ($scope, $controller, $location, addressService, cartService, orderService) {

    $controller('baseController', {$scope: $scope});//继承

    //根据登录的账号，查询该用户关联的收件人地址列表
    $scope.findAddressByUserId = function () {
        addressService.findAddressByUserId().success(function (response) {
            $scope.addressList = response;
            for (var i = 0; i < $scope.addressList.length; i++) {
                if ($scope.addressList[i].isDefault == "1") {
                    $scope.address = $scope.addressList[i];
                }
            }
            if ($scope.address == null) {//如果没有默认地址
                $scope.address = $scope.addressList[0];//设置第一个为默认选择的地址
            }
        });
        $scope.findCartList();
    }

    //选择了的需要展示的商品
    $scope.selectCartList = [];
    //展示点击结算之前 选择了的商品
    updateSelectedItem = function () {
        //$scope.selectIds = selectIds;
        $scope.selectCartList = updateItemList($scope.cartList, $scope.selectIds);
        sum();
    }
    updateItemList = function (cartList, ids) {
        var list = [];
        for (var i = 0; i < cartList.length; i++) {
            list.push({orderItemList: []});
            for (var j = 0; j < cartList[i].orderItemList.length; j++) {
                for (var k = 0; k < $scope.selectIds.length; k++) {
                    if (cartList[i].orderItemList[j].itemId == ids[k]) {
                        list[list.length - 1].orderItemList.push(cartList[i].orderItemList[j]);
                        break;
                    }
                }
            }
            if (list[list.length - 1].orderItemList.length == 0) {
                list.splice(list.length - 1, 1);
            }
        }
        return list;
    }

    $scope.changeAddrSelect = function (addr) {
        if ($scope.address != addr) {
            $scope.address = addr;
        }
    }

    //寄送地址，随着用户点击而变换
    $scope.address = null;

    //支付方式
    $scope.entity = {paymentType: '1'};
    //点击付款方式之后 更新付款方式的选择
    $scope.updatePaymentType = function (paymentType) {
        $scope.entity.paymentType = paymentType;
    }

    //查询购物车列表
    $scope.findCartList = function () {
        cartService.findCartList().success(function (response) {
            //查询到的列表数据为两段数据：list = [【redis购物车列表】,【数据库购物车列表】];
            // 使用【数据库】查询到的【后半段】数据
            $scope.cartList = response.slice(response.length/2);
            updateSelectedItem();
        });
    }

    $scope.totalNum = 0;
    $scope.totalMoney = 0.00;
    //统计商品总数和总金额
    sum = function () {
        $scope.totalNum = 0;
        $scope.totalMoney = 0.00;
        //循环遍历购物无车列表数据，获取每个商品数量和商品小计金额，做累加操作
        for (var i = 0; i < $scope.cartList.length; i++) {
            for (var j = 0; j < $scope.cartList[i].orderItemList.length; j++) {
                var index = $scope.selectIds.indexOf($scope.cartList[i].orderItemList[j].itemId);
                if (!(index == null || index < 0)) {//如果itemId存在于勾选数组中 则计算
                    $scope.totalNum += $scope.cartList[i].orderItemList[j].num;
                    $scope.totalMoney += $scope.cartList[i].orderItemList[j].totalFee;
                }

            }
        }
    }

    // $scope.selectIds =selectIds;
    $scope.selectIds = $location.search()["selectIds"].split(",").map(Number);


    //提交订单
    $scope.submitOrder = function () {
        //组装entity的其他属性
        $scope.entity.receiverAreaName = $scope.address.address;//组装收货人地区名称
        $scope.entity.receiverMobile = $scope.address.mobile;//组装收货人手机
        $scope.entity.receiver = $scope.address.contact;//组装收货人姓名

        orderService.add($scope.entity, $scope.selectIds).success(function (response) {//
            if (response.success) {
                //订单提交成功，跳转至支付页面
                location.href = "pay.html"
            } else {
                alert(response.message);
            }
        })
    }

});
