//控制层
module.controller('cartController', function ($scope, $controller, $timeout, cartService) {

    $controller('baseController', {$scope: $scope});//继承

    //查询购物车列表
    $scope.findCartList = function () {
        cartService.findCartList().success(function (response) {
            $scope.oldCartList = response.slice(0, response.length / 2);//上一次查询得到的cartList，用来【提示降价（改变）】
            $scope.cartList = response.slice(response.length / 2);//最新的cartList，用来【展示】
            //更新由于angularJs绑定模型数据的页面刷新机制造成勾选框恢复成未选择状态的问题
            //angularJs绑定了模型数据后，会在模型变量改变之后，最后统一刷新页面，此时不管如何改变复选框，都会造成复选框恢复未选择状态的结果
            //故设置一延迟时间，可粗略解决此问题
            $timeout(function () {
                $scope.updateBox();//更新已经选择了的复选框的状态
            }, 50)
            $scope.sum();
        });
    }
    /**
     *
     * @param itemId
     * @param num
     */

    //添加商品至购物车
    $scope.addItemToCartList = function (itemId, num) {
        cartService.addItemToCartList(itemId, num).success(function (response) {
            if (response.success) {//如果添加成功
                if (response.message == "updateSelectIds") {//如果需要清除selectIds中的itemId
                    $scope.selectIds.splice($scope.selectIds.indexOf(itemId), 1);
                }
                //重新查询购物车列表数据
                $scope.findCartList();
            } else {//如果添加失败
                alert(response.message);
            }
        });
    }
    changeItemsSelect = function (orderItemList) {
        for (var i = 0; i < orderItemList.length; i++) {
            var index = $scope.selectIds.indexOf(orderItemList[i].itemId);
            var itemObj = document.getElementById(orderItemList[i].itemId);
            //如果商品选择数组中存在此id 则选择上
            if (!(index == null || index < 0)) {
                itemObj.checked = true;
            } else {
                itemObj.checked = false;
            }
        }
    }
    $scope.deleItemsFromCartList = function (itemId, num) {
        if (confirm("你确定要删除此项商品吗？")) {
            $scope.addItemToCartList(itemId, num);
        }
    }

    $scope.selectItems = function ($event, sellerId) {
        if ($event.target.checked) {//如果勾选
            //改变商家下所有的勾选状态为true
            changeSellerItems($scope.cartList, sellerId, $scope.selectIds, true);
        } else {//如果是取消勾选
            //改变商家下所有的勾选状态为false
            changeSellerItems($scope.cartList, sellerId, $scope.selectIds, false);
        }
    }


    //根据商家下商品的【勾选】状态 改变指定商家复选框状态
    $scope.changeSellerSelect = function (sellerId) {
        //判断商家下的复选框是否全部勾选
        var cartList = $scope.cartList;
        var flag = selectItemsCount(cartList, sellerId);
        //选择商家的复选框
        var sellerObj = document.getElementById(sellerId);
        if (flag) {//如果全部选择了
            //更新商家复选框勾选状态为【勾选】
            sellerObj.checked = true;
        } else {//如果没有全部选择
            //更新商家复选框勾选状态为【未勾选】
            sellerObj.checked = false;
        }
    };
    //计算 点击商家下商品的复选框后，选择了该商家的商品的数量如果不等于该商家商品列表长度 则返回false 否则true
    selectItemsCount = function (cartList, sellerId) {
        for (var i = 0; i < cartList.length; i++) {
            if (cartList[i].sellerId == sellerId) {
                var count = 0;//商家下勾选商品的数量
                for (var j = 0; j < cartList[i].orderItemList.length; j++) {
                    var itemId = cartList[i].orderItemList[j].itemId;
                    var index = $scope.selectIds.indexOf(itemId);
                    if (!(index == null || index < 0)) {//如果是勾选的 则count+1
                        count += 1;
                    }
                }
                if (count != cartList[i].orderItemList.length) {
                    return false;
                }
                return true;
            }
        }
        return false;//如果没有找到商家id，则返回false
    }
    //检测selectIds的选择状态 如果变化之后 长度等于全部商品的总长度 则勾选全选复选框
    $scope.$watch("selectIds.length", function (newValue, oldValue) {
        if (newValue != oldValue) {
            //如果是最后一个分类 则根据分类id查询模板id
            //计算购物车所有商品sku的个数
            var cartListLength = countcartListItems($scope.cartList);
            //如果选择的商品长度等于全部商品的总长度 则勾选全选复选框
            if ($scope.selectIds.length == cartListLength) {
                $scope.selectAllBoxFlag = "1";
                // var checkObj = document.getElementById("selectAll1");
                // checkObj.checked=true;
            } else {
                $scope.selectAllBoxFlag = "0";
            }
            //改变总价格
            $scope.sum();
        }
    });

    $scope.updateBox = function () {
        for (var i = 0; i < $scope.cartList.length; i++) {
            //更新【商品】的复选框状态
            changeItemsSelect($scope.cartList[i].orderItemList);
            //更新【商家】的复选框状态
            $scope.changeSellerSelect($scope.cartList[i].sellerId);
        }

        //更新【全选复选框】的复选框状态
        var cartListLength = countcartListItems($scope.cartList);
        if ($scope.selectIds.length == cartListLength) {
            $scope.selectAllBoxFlag = "1";
            // var checkObj = document.getElementById("selectAll1");
            // checkObj.checked=true;
        } else {
            $scope.selectAllBoxFlag = "0";
        }

    };


    $scope.totalNum = 0;
    $scope.totalMoney = 0.00;
    //统计商品总数和总金额
    $scope.sum = function () {
        $scope.totalNum = 0;
        $scope.totalMoney = 0.00;
        //循环遍历购物无车列表数据，获取每个商品数量和商品小计金额，做累加操作
        for (var i = 0; i < $scope.cartList.length; i++) {
            for (var j = 0; j < $scope.cartList[i].orderItemList.length; j++) {
                var index = $scope.selectIds.indexOf($scope.cartList[i].orderItemList[j].itemId);
                if (!(index == null || index < 0)) {//如果itemId存在于勾选数组中 则计算
                    $scope.totalNum += $scope.cartList[i].orderItemList[j].num;
                    $scope.totalMoney += $scope.cartList[i].orderItemList[j].totalFee;
                }

            }
        }
    }


    //计算购物车所有商品sku的个数
    countcartListItems = function (cartList) {
        var count = 0;
        for (var i = 0; i < cartList.length; i++) {
            count += cartList[i].orderItemList.length;
        }
        return count;
    }


    changeSellerItems = function (cartList, sellerId, selectIds, flag) {//flag为true时全部勾选，false时为全部取消勾选
        //遍历商家
        for (var i = 0; i < cartList.length; i++) {
            //如果是选择的商家，则遍历商品列表
            if (cartList[i].sellerId == sellerId) {
                for (var j = 0; j < cartList[i].orderItemList.length; j++) {
                    var itemId = cartList[i].orderItemList[j].itemId;
                    var inputObj = document.getElementById(itemId);
                    if (inputObj.checked != flag) {//如果当前复选框勾选状态改变之后与flag相等
                        inputObj.checked = flag;//更改勾选状态
                        if (flag) {//如果是需要全部勾选，则将id加入选择数组
                            selectIds.push(cartList[i].orderItemList[j].itemId);
                        } else {//如果是需要全部取消勾选，则将id移除选择数组
                            selectIds.splice(selectIds.indexOf(cartList[i].orderItemList[j].itemId), 1);
                        }
                    }
                }
                break;
            }
        }
    }
    //
    $scope.selectAll = function ($event) {
        //改变商品复选框状态
        var boxes = document.getElementsByName("selectOne");//.checked = true
        for (var i = 0; i < boxes.length; i++) {
            boxes[i].checked = $event.target.checked;
        }
        if ($event.target.checked) {//如果是全部勾选
            //循环赋值选择数组
            var sum = 0;
            for (var i = 0; i < $scope.cartList.length; i++) {
                for (var j = 0; j < $scope.cartList[i].orderItemList.length; j++, sum++) {
                    $scope.selectIds[sum] = $scope.cartList[i].orderItemList[j].itemId;
                }
                //改变商家复选框状态
                $scope.changeSellerSelect($scope.cartList[i].sellerId);
            }
        } else {//如果是全部取消勾选
            $scope.selectIds = [];
            //改变商家复选框状态
            for (var i = 0; i < $scope.cartList.length; i++) {
                $scope.changeSellerSelect($scope.cartList[i].sellerId);
            }
        }

    }

    $scope.account = function () {
        if ($scope.selectIds.length > 0) {
            location.href = "getOrderInfo.html#?selectIds=" + $scope.selectIds;
        } else {
            alert("请选择要购买的物品！");
        }
    }

    /**
     * 购物车页面，鼠标悬浮或移出在sku的title时，需要变化的量
     */
    $scope.editSku = -1;

    $scope.changeEditSku = function (itemId) {
        if (itemId >= 0) {
            cartService.selectTitleByItemId(itemId).success(function (response) {
                $scope.skuOptions = response;
            })
        }
        $scope.editSku = itemId;
    }


    //商品的规格下拉选项变化之后 改变购物车
    $scope.changeCartList = function(newValue,oldValue){
        if (newValue!=oldValue){
            cartService.changeCartList(newValue,oldValue).success(function (response) {
                if (response.success){
                    $scope.findCartList();
                }else {
                    alert(response.message);
                }
            });
        }
    }

    $scope.skuImg = -1;
    //鼠标悬停在下拉规格选项列表后，显示其图片
    $scope.showSkuImg = function (itemId) {
        $scope.skuImg = itemId;
    }



});
