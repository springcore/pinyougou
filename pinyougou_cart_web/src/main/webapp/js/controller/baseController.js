module.controller("baseController",function ($scope) {
    $scope.paginationConf={
        currentPage:1,  //当前页
        totalItems:10,  //总记录数
        itemsPerPage:10,  //每页记录数
        perPageOptions:[10,20,30,40,50], //分页选项，
        onChange:function () {  //改变之后触发的方法
            $scope.loadList();
        }
    };
    $scope.loadList=function(){
        $scope.search($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage);
    };

    //    定义批量删除的数组
    $scope.selectIds=[];
    //改变数组的函数
    $scope.changeArray=function ($event,id) {
        //$event.target 选的是复选框
        if ($event.target.checked){
            $scope.selectIds.push(id);
        }else {
            var index = $scope.selectIds.indexOf(id);
            $scope.selectIds.splice(index,1);
        }
    };

    $scope.getStringByJson=function (jsonString, key) {
        var jsonArr = JSON.parse(jsonString);
        var str="";
        for (var i = 0; i < jsonArr.length; i++) {
            if (i>0){
                str+=", "+jsonArr[i][key];
            }else {
                str=jsonArr[i][key];
            }
        }
        return str;
    };

});