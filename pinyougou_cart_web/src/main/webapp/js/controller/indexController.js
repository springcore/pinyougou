module.controller("indexController",function ($scope ,$controller,loginService) {

    $controller("baseController",{$scope:$scope});

    //获取登陆人用户名

    $scope.getLoginName = function () {
        loginService.getLoginName().success(function (response) {
            $scope.loginName=response.loginName;
        })
    }

});