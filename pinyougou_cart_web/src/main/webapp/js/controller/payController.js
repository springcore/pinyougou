//控制层
module.controller('payController', function ($scope, $controller,$location,$interval,payService) {

    $controller('baseController', {$scope: $scope});//继承

    //生成二维码啊所需参数 【订单号】 【支付金额】 【支付链接】
    $scope.createNative = function () {
        payService.createNative().success(function (response) {
            //接收响应结果 生成二维码
            $scope.maaap = response;
            $scope.out_trade_no = response.out_trade_no; //订单号
            $scope.total_fee = (response.total_fee/100).toFixed(2);
            $scope.code_url = response.code_url; //支付链接

            //生成二维码即可
            var qr = window.qr = new QRious({
                element:document.getElementById("qrious"),
                size:300,
                value:$scope.code_url,
                level:'H'
            })
            $scope.count = 0;//计数，如果累加到100次（5分钟后），停止查询支付详情，并提醒用户刷新
            $scope.intervalId = $interval(function () {//3秒一次，查询订单状态
                $scope.queryPayStatus();
                $scope.count++;
            },3000);
        })
        document.getElementById("translucent").hidden=true;//隐藏遮挡二维码图片
    }

    $scope.$watch("count",function (newValue,oldValue) {
        if (newValue!=oldValue){
            if (newValue>100){//（5分钟后），停止查询支付详情，并提醒用户刷新
                $interval.cancel($scope.intervalId);//停止id为intervalId的计时器
                $scope.QRcodeMessage = "二维码失效，请点击刷新";
                document.getElementById("translucent").hidden=false;//显示遮挡二维码图片
            }
        }
    })

    //查询支付状态
    $scope.queryPayStatus = function () {
        payService.queryPayStatus($scope.out_trade_no).success(function (response) {
            if (response.success){//如果不需要跳转支付失败页面
                if (response.message=="payTimeOut"){//如果超时
                    document.getElementById("translucent").hidden=false;//显示遮挡二维码图片
                    $scope.QRcodeMessage = "超出支付时间，点击重新生成二维码";
                    $interval.cancel($scope.intervalId);//停止id为intervalId的计时器
                }else if (response.message=="SUCCESS") {//如果是支付成功
                    location.href = "paysuccess.html#?out_trade_no="+$scope.out_trade_no;//跳转至支付成功页面
                }
            }else {//如果需要跳转支付失败页面
                location.href="payfail.html#?message="+response.message;
            }
        })
    }

    //获取支付金额
    $scope.getMoney = function () {
        payService.getPayLogMoneyByOutTradeNo($location.search()["out_trade_no"]).success(function (response) {
            $scope.money = response;
        });
    }

    $scope.getFailMessage = function () {
        $scope.failMessage = $location.search()["message"];
    }

});
