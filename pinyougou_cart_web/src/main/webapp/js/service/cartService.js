//服务层
module.service('cartService', function ($http) {

    this.findCartList = function () {
        return $http.get("cart/findCartList.do");
    }

    this.addItemToCartList = function (itemId,num) {
        return $http.get("cart/addItemToCartList.do?itemId="+itemId+"&num="+num);
    }

    this.selectTitleByItemId = function (itemId) {
        return $http.get("cart/selectTitleByItemId.do?itemId="+itemId);
    }

    this.changeCartList = function (newValue, oldValue) {
        return $http.get("cart/changeCartList.do?newItemId="+newValue+"&oldItemId="+oldValue);
    }
});
