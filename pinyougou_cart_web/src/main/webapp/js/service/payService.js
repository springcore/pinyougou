//服务层
module.service('payService', function ($http) {
    //生成支付二维码所需要的 【订单号】 【支付金额】 【支付链接】
    this.createNative = function () {
        return $http.get('pay/createNative.do');
    }

    //查询支付状态
    this.queryPayStatus = function (out_trade_no) {
        return $http.get('pay/queryPayStatus.do?out_trade_no='+out_trade_no);
    }
    this.getPayLogMoneyByOutTradeNo = function (out_trade_no) {
        return $http.get('pay/getPayLogMoneyByOutTradeNo.do?out_trade_no='+out_trade_no);
    }
});
