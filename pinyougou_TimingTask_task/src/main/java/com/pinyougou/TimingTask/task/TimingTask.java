package com.pinyougou.TimingTask.task;


import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.mapper.TbSeckillOrderMapper;
import com.pinyougou.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class TimingTask {


    @Autowired
    private TbSeckillOrderMapper seckillOrderMapper;
    @Autowired
    private TbOrderMapper orderMapper;


    @Scheduled(cron = "0/60 * * * * ?")//每隔60秒执行一次
    public void updateStaToOrder() {


        //普通订单 30分钟 关闭订单

        TbOrderExample exampleOrder=new TbOrderExample();
        TbOrderExample.Criteria criteriaOrder = exampleOrder.createCriteria();
        criteriaOrder.andStatusEqualTo("1");//有效订单

        List<TbOrder> tbOrders = orderMapper.selectByExample(exampleOrder);
        for (TbOrder tbOrder : tbOrders) {
            Date orderCreateTime = tbOrder.getCreateTime();
            Date date = new Date();
            if ((date.getTime()-orderCreateTime.getTime())>(30*60*1000)){
                tbOrder.setStatus("6");
                orderMapper.updateByPrimaryKey(tbOrder);
                System.out.println("关闭普通订单，id："+tbOrder.getOrderId());
            }
        }

    }


    @Scheduled(cron = "0/60 * * * * ?")//每隔60秒执行一次
    public void updateStaToSeckillGoods() {

        //秒杀， 关闭订单 1.  5分钟 2. 下单时间

        TbSeckillOrderExample example=new TbSeckillOrderExample();
        TbSeckillOrderExample.Criteria criteria = example.createCriteria();
        criteria.andStatusEqualTo("1");
        List<TbSeckillOrder> tbSeckillOrders = seckillOrderMapper.selectByExample(example);
        for (TbSeckillOrder tbSeckillOrder : tbSeckillOrders) {
            Date createTime = tbSeckillOrder.getCreateTime();
            long oldTime = createTime.getTime();//获取订单生成时间
            long newTime = new Date().getTime();//获取当前时间
            if ((newTime-oldTime)>(5*60*1000)){//大于五分钟
                tbSeckillOrder.setStatus("6");//关闭订单
                seckillOrderMapper.updateByPrimaryKey(tbSeckillOrder);
                System.out.println("关闭秒杀订单，id："+tbSeckillOrder.getId());
            }
        }


    }
}
