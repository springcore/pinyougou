package com.pinyougou.seckill.task;

import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillGoodsExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class SeckillTask {

    @Autowired
    private TbSeckillGoodsMapper seckillGoodsMapper;

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 将数据库秒杀数据同步到redis缓存
     */
    //@Scheduled(cron = "0 55 9 * * ?")//每天9点55分钟执行
    @Scheduled(cron = "0/10 * * * * ?")//每隔10秒执行一次
    public void synchronizeSeckillGoodsToRedis(){
        /**
         *   审核通过
             有库存
             当前时间大于开始时间,并小于秒杀结束时间  即：正在秒杀的商品
         */
        TbSeckillGoodsExample example = new TbSeckillGoodsExample();
        example.createCriteria().
                andStatusEqualTo("1")//审核通过
                .andStockCountGreaterThan(0).//有库存
                andStartTimeLessThanOrEqualTo(new Date()).//当前时间大于开始时间
                andEndTimeGreaterThanOrEqualTo(new Date());//当前时间小于秒杀结束时间
        List<TbSeckillGoods> seckillGoodsList = seckillGoodsMapper.selectByExample(example);
        //将每件秒杀商品一个个存入redis目的是方便基于商品id获取秒杀商品内容，展示在秒杀商品详情页
        for (TbSeckillGoods seckillGoods : seckillGoodsList) {
            redisTemplate.boundHashOps("seckill_goods").put(seckillGoods.getId(),seckillGoods);

            //基于redis队列，记录当前秒杀商品还剩多少个  5
            Integer stockCount = seckillGoods.getStockCount();//获取当前商品剩余库存数
            for(int i=0;i<stockCount;i++){  //[{},{},{},{},{}]     [1,1,1,1,1,1]
                redisTemplate.boundListOps("sekill_goods_queue_"+seckillGoods.getId()).leftPush(seckillGoods.getId());
            }
        }

        System.out.println("synchronizeSeckillGoodsToRedis finish ........");
    }


}
