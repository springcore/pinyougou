 //控制层 
app.controller('itemCatController' ,function($scope,$controller   ,itemCatService,typeTemplateService){
	
	$controller('baseController',{$scope:$scope});//继承
	
    //读取列表数据绑定到表单中  
	$scope.findAll=function(){
		itemCatService.findAll().success(
			function(response){
				$scope.list=response;
			}			
		);
	}    
	
	//分页
	$scope.findPage=function(page,rows){			
		itemCatService.findPage(page,rows).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}
	
	//查询实体 
	$scope.findOne=function(id){				
		itemCatService.findOne(id).success(
			function(response){
				$scope.entity= response;					
			}
		);				
	}
	
	//保存 
	$scope.save=function(){				
		var serviceObject;//服务层对象  				
		if($scope.entity.id!=null){//如果有ID
			serviceObject=itemCatService.update( $scope.entity ); //修改  
		}else{
			//指定新增分类的父id
			$scope.entity.parentId=$scope.parentId;
			serviceObject=itemCatService.add( $scope.entity  );//增加 
		}				
		serviceObject.success(
			function(response){
				if(response.success){
					//重新查询 
                    $scope.findByParentId($scope.parentId);
				}else{
					alert(response.message);
				}
			}		
		);				
	}
	
	 
	//批量删除 
	$scope.dele=function(){			
		//获取选中的复选框			
		itemCatService.dele( $scope.selectIds ).success(
			function(response){
				if(response.success){
					$scope.reloadList();//刷新列表
				}						
			}		
		);				
	}
	
	$scope.searchEntity={};//定义搜索对象 
	
	//搜索
	$scope.search=function(page,rows){			
		itemCatService.search(page,rows,$scope.searchEntity).success(
			function(response){
				$scope.list=response.rows;	
				$scope.paginationConf.totalItems=response.total;//更新总记录数
			}			
		);
	}

	//定义当前基本的父id
	$scope.parentId=0;

    //查询下级
    $scope.findByParentId=function(parentId){
        $scope.parentId=parentId;
        itemCatService.findByParentId(parentId).success(
            function(response){
                $scope.list=response;
            }
        );
    }
	//初始化查询一级分类的列表数据
    $scope.grade=1;
    //设置页面展示的分类级别
	$scope.setGrade=function (grade) {
        $scope.grade=grade;
    }

    //实现面包屑导航栏效果
	$scope.selectItemCatList=function (entity_p) {
		//当分类列表展示的是一级分类数据时
		if( $scope.grade==1){
			$scope.entity_1=null;
			$scope.entity_2=null;
		}

        //当分类列表展示的是二级分类数据时
        if( $scope.grade==2){
            $scope.entity_1=entity_p;
            $scope.entity_2=null;
        }

        //当分类列表展示的是三级分类数据时
        if( $scope.grade==3){
            $scope.entity_2=entity_p;
        }

        //点击导航栏超链接，需要查询当前分类子分类
        $scope.findByParentId(entity_p.id);
    }

    //查询分类关联的模板
    $scope.selectTypeTemplateList=function () {
        typeTemplateService.findAll().success(function (response) {
            $scope.typeTemplateList=response;
        })
    }
	

});	
