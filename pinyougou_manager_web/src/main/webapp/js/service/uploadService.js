//服务层
app.service('uploadService',function($http){

	//文件上传
	this.uploadFile=function () {

        //结合angularjs和html5表单对象实现文件上传功能
        var formData = new FormData();
        //获取页面选择的文件对象
        //参数一：文件上传方法，后端接收文件对象的变量名
        //参数二：获取的上传文件  file.files[0] 写法中的file对象指的是：<input type="file" id="file" />	中的id属性值
        formData.append("file",file.files[0]);

        //发起请求，提交上传文件
        return $http({
            url:"../upload/uploadFile.do",
            method:"post",
            data:formData,
            headers : {'Content-Type' : undefined}, //上传文件必须是这个类型，默认text/plain，设置为enctype="multipart/form-data"
            transformRequest : angular.identity  //对整个表单进行二进制序列化
        });
    }

});
