package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbSpecification;
import com.pinyougou.sellergoods.service.SpecificationService;
import entity.PageResult;
import entity.Result;
import groupEntity.Specification;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/*@Controller
@ResponseBody*/
@RestController //相当于@Controller+@ResponseBody
@RequestMapping("/specification")
public class SpecificationController {

    @Reference
    private SpecificationService specificationService;

    /**
     * 条件分页查询
     * specification组装查询条件的对象
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbSpecification specification,Integer pageNum,Integer pageSize){
        return specificationService.search(specification,pageNum,pageSize);
    }

    /**
     * 新增
     *  成功与否的标记  以及提示信息
     */
    @RequestMapping("/add")
    //@RequestBody作用：完成页面对象的属性与后端实体对象属于映射
    public Result add(@RequestBody Specification specification){
        try {
            specificationService.add(specification);
            return new Result(true,"保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"保存失败");
        }
    }

    /**
     * 修改时数据回显操作
     */
    @RequestMapping("/findOne")
    public Specification findOne(Long id){
        return specificationService.findOne(id);
    }

    /**
     * 修改
     *  成功与否的标记  以及提示信息
     */
    @RequestMapping("/update")
    //@RequestBody作用：完成页面对象的属性与后端实体对象属于映射
    public Result update(@RequestBody Specification specification){
        try {
            specificationService.update(specification);
            return new Result(true,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"修改失败");
        }
    }

    /**
     * 删除
     *  成功与否的标记  以及提示信息
     */
    @RequestMapping("/delete")
    //@RequestBody作用：完成页面对象的属性与后端实体对象属于映射
    public Result delete(Long[] ids){
        try {
            specificationService.delete(ids);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除失败");
        }
    }

    /**
     * 模板关联的规格列表
     *   注意：当可以使用实体类封装数据时，也可以基于map集合封装
     */
    @RequestMapping("/selectSpecOptions")
    public List<Map> selectSpecOptions(){
        return specificationService.selectSpecOptions();
    }

}
