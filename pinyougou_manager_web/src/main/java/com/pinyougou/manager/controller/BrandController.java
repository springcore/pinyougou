package com.pinyougou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.pojo.TbBrand;
import com.pinyougou.sellergoods.service.BrandService;
import entity.PageResult;
import entity.Result;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/*@Controller
@ResponseBody*/
@RestController //相当于@Controller+@ResponseBody
@RequestMapping("/brand")
public class BrandController {

    //调用dubbo服务， 去注册中心找服务
    @Reference
    private BrandService brandService;

    /**
     * 查询所有品牌列表数据
     * @return
     */
    //[{},{}]
    @RequestMapping("/findAll")
    public List<TbBrand> findAll(){
        return brandService.findAll();
    }
    /**
     * 分页查询品牌列表数据
     * 响应结果：当前页列表集合、总条数
     * 请求参数：当前页、每页查询记录数
     * {}
     */
    @RequestMapping("/findPage")
    public PageResult findPage(Integer pageNum,Integer pageSize){
        return brandService.findPage(pageNum,pageSize);
    }

    /**
     * 新增品牌
     *  成功与否的标记  以及提示信息
     */
    @RequestMapping("/add")
    //@RequestBody作用：完成页面对象的属性与后端实体对象属于映射
    public Result add(@RequestBody TbBrand brand){
        try {
            brandService.add(brand);
            return new Result(true,"保存成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"保存失败");
        }
    }

    /**
     * 修改时数据回显操作
     */
    @RequestMapping("/findOne")
    public TbBrand findOne(Long id){
        return brandService.findOne(id);
    }

    /**
     * 修改品牌
     *  成功与否的标记  以及提示信息
     */
    @RequestMapping("/update")
    //@RequestBody作用：完成页面对象的属性与后端实体对象属于映射
    public Result update(@RequestBody TbBrand brand){
        try {
            brandService.update(brand);
            return new Result(true,"修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"修改失败");
        }
    }

    /**
     * 删除品牌
     *  成功与否的标记  以及提示信息
     */
    @RequestMapping("/delete")
    //@RequestBody作用：完成页面对象的属性与后端实体对象属于映射
    public Result delete(Long[] ids){
        try {
            brandService.delete(ids);
            return new Result(true,"删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"删除失败");
        }
    }

    /**
     * 条件分页查询
     * specification组装查询条件的对象
     */
    @RequestMapping("/search")
    public PageResult search(@RequestBody TbBrand brand, Integer pageNum, Integer pageSize){
        return brandService.search(brand,pageNum,pageSize);
    }

    /**
     * 品牌模板关联的品牌列表
     *   注意：当可以使用实体类封装数据时，也可以基于map集合封装
     */
    @RequestMapping("/selectBrandOptions")
    public List<Map> selectBrandOptions(){
        return brandService.selectBrandOptions();
    }
}
