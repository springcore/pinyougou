package com.pinyougou.seckill.service.impl;

import com.pinyougou.mapper.TbSeckillGoodsMapper;
import com.pinyougou.mapper.TbSeckillOrderMapper;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.pojo.TbSeckillOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import util.IdWorker;

import java.util.Date;
import java.util.Map;

@Component
public class CreateOrder implements Runnable{

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private TbSeckillOrderMapper seckillOrderMapper;

    @Autowired
    private TbSeckillGoodsMapper seckillGoodsMapper;

    @Override
    public void run() {
        //从redis队列中获取秒杀下单任务
        Map<String,Object> map = (Map<String, Object>) redisTemplate.boundListOps("sekill_order_queue").rightPop();
        Long seckillGoodsId = (Long) map.get("seckillGoodsId");
        String userId = (String) map.get("userId");

        //获取秒杀商品   超卖  5  50
        TbSeckillGoods seckillGoods= (TbSeckillGoods) redisTemplate.boundHashOps("seckill_goods").get(seckillGoodsId);

        //生成秒杀订单
        TbSeckillOrder seckillOrder = new TbSeckillOrder();
        /* `id` bigint(20) NOT NULL COMMENT '主键',
          `seckill_id` bigint(20) DEFAULT NULL COMMENT '秒杀商品ID',
          `money` decimal(10,2) DEFAULT NULL COMMENT '支付金额',  //秒杀商品的秒杀价格
          `user_id` varchar(50) DEFAULT NULL COMMENT '用户',
          `seller_id` varchar(50) DEFAULT NULL COMMENT '商家', //秒杀商品关联的商家id值
          `create_time` datetime DEFAULT NULL COMMENT '创建时间',
          `status` varchar(1) DEFAULT NULL COMMENT '状态', // 1 未支付*/
        long seckillOrderId = idWorker.nextId();
        seckillOrder.setId(seckillOrderId);
        seckillOrder.setSeckillId(seckillGoodsId);
        seckillOrder.setMoney(seckillGoods.getCostPrice());
        seckillOrder.setUserId(userId);
        seckillOrder.setSellerId(seckillGoods.getSellerId());
        seckillOrder.setCreateTime(new Date());
        seckillOrder.setStatus("1");// 1 未支付
        //秒杀下单
        seckillOrderMapper.insert(seckillOrder);
        //秒杀下单，要将redis中秒杀商品库存减一
        seckillGoods.setStockCount(seckillGoods.getStockCount()-1);

        //在redis中，记录当前用户购买过当前秒杀商品了
        //redisTemplate.boundValueOps(userId).set(seckillGoodsId);
        redisTemplate.boundSetOps("seckill_goods_"+seckillGoodsId).add(userId);

        //当秒杀下单完成后，排队人数减一
        redisTemplate.boundValueOps("seckill_goods_queue_"+seckillGoodsId).increment(-1);

        //同步数据库的时机：当库存为0或者秒杀结束
        if(seckillGoods.getStockCount()==0 || (new Date().getTime()>seckillGoods.getEndTime().getTime())){
            //更新秒杀商品库存到数据库
            seckillGoodsMapper.updateByPrimaryKey(seckillGoods);
            //清除redis中秒杀商品
            redisTemplate.boundHashOps("seckill_goods").delete(seckillGoodsId);
        }else {
            //秒杀下单，但是秒杀还没有结束，此时更新redis中秒杀商品库存
            redisTemplate.boundHashOps("seckill_goods").put(seckillGoodsId,seckillGoods);
        }
    }
}
