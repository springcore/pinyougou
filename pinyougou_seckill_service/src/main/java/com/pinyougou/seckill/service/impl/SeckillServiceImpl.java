package com.pinyougou.seckill.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.pojo.TbSeckillGoods;
import com.pinyougou.seckill.service.SeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class SeckillServiceImpl implements SeckillService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ThreadPoolTaskExecutor executor;

    @Autowired
    private CreateOrder createOrder;

    @Override
    public List<TbSeckillGoods> selectSeckillGoodsFromRedis() {
        return redisTemplate.boundHashOps("seckill_goods").values();
    }

    @Override
    public TbSeckillGoods findOne(Long seckillGoodsId) {
        return (TbSeckillGoods) redisTemplate.boundHashOps("seckill_goods").get(seckillGoodsId);
    }

    @Override
    public void submitSeckillOrder(Long seckillGoodsId, String userId) {
        //判断当前用户是否购买过当前商品
        Boolean member = redisTemplate.boundSetOps("seckill_goods_" + seckillGoodsId).isMember(userId);
        if (member) {
            throw new RuntimeException("同一用户只能抢购一个同款商品");
        }

        //进入一个线程，秒杀排队人数加一
        redisTemplate.boundValueOps("seckill_goods_queue_"+seckillGoodsId).increment(1);

        //限制库存，解决超卖问题，先从redis队列中获取商品信息  []
        Object obj = redisTemplate.boundListOps("sekill_goods_queue_" + seckillGoodsId).rightPop();
        if(obj==null){
            throw new RuntimeException("很遗憾，秒杀商品售罄");
        }

        //获取秒杀商品   超卖  5  50
        TbSeckillGoods seckillGoods= (TbSeckillGoods) redisTemplate.boundHashOps("seckill_goods").get(seckillGoodsId);

        /*if(seckillGoods==null || seckillGoods.getStockCount()<=0 ){
            throw new RuntimeException("很遗憾，秒杀商品售罄");
        }*/

        //获取排队人数
        Long size = redisTemplate.boundValueOps("seckill_goods_queue_" + seckillGoodsId).size();
        //当排队人数大于秒杀商品库存指定值（例如：20），提醒排队人数过多
        Integer stockCount = seckillGoods.getStockCount();
        if(size>(stockCount+20)){
            throw new RuntimeException("排队人数过多");
        }

        Map<String,Object> map = new HashMap<>();
        map.put("seckillGoodsId",seckillGoodsId);
        map.put("userId",userId);

        //将秒杀下单的操作，作为任务存入redis队列中
        redisTemplate.boundListOps("sekill_order_queue").leftPush(map);

        //开启多线程执行秒杀订单参数
        executor.execute(createOrder);

    }
}
