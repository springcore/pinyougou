package com.pinyougou.sellergoods.service;

import com.pinyougou.pojo.TbSpecification;
import entity.PageResult;
import groupEntity.Specification;

import java.util.List;
import java.util.Map;

public interface SpecificationService {
    /**
     * 条件分页查询
     * specification组装查询条件的对象
     */
    PageResult search(TbSpecification specification, Integer pageNum, Integer pageSize);

    void add(Specification specification);

    Specification findOne(Long id);

    void update(Specification specification);

    void delete(Long[] ids);

    List<Map> selectSpecOptions();
}
