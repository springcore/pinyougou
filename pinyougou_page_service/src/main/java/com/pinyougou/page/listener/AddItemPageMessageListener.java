package com.pinyougou.page.listener;

import com.pinyougou.page.service.PageService;
import com.pinyougou.pojo.TbItem;
import freemarker.template.Configuration;
import freemarker.template.Template;
import groupEntity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import java.io.FileWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddItemPageMessageListener implements MessageListener {

    @Autowired
    private FreeMarkerConfigurer freemarkerConfig;

    @Autowired
    private PageService pageService;

    @Override
    public void onMessage(Message message) {
        //商品上架，同步生成上架商品静态页

        try {
            TextMessage textMessage = (TextMessage)message;
            //下架消息是商品id值
            String goodsId = textMessage.getText();

//        第一步：创建一个 Configuration 对象
            Configuration configuration = freemarkerConfig.getConfiguration();
//        第四步：加载一个模板，创建一个模板对象。
            Template template = configuration.getTemplate("item.ftl");
//        第五步：创建一个模板使用的数据集，可以是 pojo 也可以是 map。一般是 Map。
            Goods goods = pageService.findOne(Long.parseLong(goodsId));
//        第六步：创建一个 Writer 对象，一般创建一 FileWriter 对象，指定生成的文件名。
            List<TbItem> itemList = goods.getItemList();
            for (TbItem item : itemList) {
                Map<String,Object> map = new HashMap<>();
                map.put("goods",goods);
                map.put("item",item);
                Writer out = new FileWriter("F:\\item83\\"+item.getId()+".html");
//        第七步：调用模板对象的 process 方法输出文件。
                template.process(map,out);
//        第八步：关闭流
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
