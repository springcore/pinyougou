package com.pinyougou.cart.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import com.pinyougou.pojo.TbItemExample;
import com.pinyougou.pojo.TbOrderItem;
import groupEntity.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
public class CartServiceImpl implements CartService {

    @Autowired
    private TbItemMapper itemMapper;

    @Override
    //添加商品至购物车
    public Map<String,Object> addItemToCartList(List<Cart> cartList, Long itemId, Integer num) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("status", "0");//status为0时，不需要splice前端selectIds中的itemId，反之，则需要
        //获取该商品的商家id
        TbItem item = itemMapper.selectByPrimaryKey(itemId);
        if (item==null){
            throw new RuntimeException("找不到商品id为"+itemId+"的商品！");
        }
        if (!"1".equals(item.getStatus())){//如果商品的状态不是有效状态
            throw new RuntimeException("该商品已经失效，请选择其它商品！");
        }
        String sellerId = item.getSellerId();
        //遍历购物车列表，判断【商家】是否已经存在,若存在，则返回该商家
        Cart cart = searchCartBySellerId(cartList,sellerId);
        if (cart==null){//如果没有找到商家 则新建一个购物车
            cart = new Cart();
            cart.setSellerId(sellerId);
            //构建购物车商品明细列表
            List<TbOrderItem> orderItemList = new ArrayList<>();
            //构建购物车商品对象
            TbOrderItem orderItem = createOrderItem(item,num);
            orderItemList.add(orderItem);
            cart.setOrderItemList(orderItemList);
            cartList.add(cart);
        }else {//如果找到商家[cart]
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            //根据商品id查询【商品】是否已经存在该商家的商品列表中，如果存在 返回该对象
            TbOrderItem orderItem = searchOrderItemByItemId(orderItemList,item.getId());
            if (orderItem==null){ //如果【商品】不存在商家列表中 创建新的orderItem对象 并加入商家列表
                //创建新的orderItem对象
                orderItem = createOrderItem(item, num);
                //加入商家列表
                orderItemList.add(orderItem);
            }else {//如果【商品】存在商家列表 商品数+num
                orderItem.setNum(orderItem.getNum()+num);
                //重新计算金额小计
                orderItem.setTotalFee(new BigDecimal(orderItem.getNum()*orderItem.getPrice().doubleValue()));
                //商品数量有可能加减，如果到0 则删除商品
                if (orderItem.getNum()<=0){//移除商品
                    orderItemList.remove(orderItem);
                    map.put("status", "1");//status为1时，需要清除前端selectIds中的orderItem.getItemId()
                }
                if (orderItemList.size()==0){//如果【商家】下的商品列表全都减完 则删除该商家的商品列表
                    cartList.remove(cart);
                }
            }
        }
        map.put("cartList", cartList);
        return map;
    }


    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    //没有登录时，基于sessionId记录购物车列表数据7天
    //登陆后 删除基于sessionId存储的购物车数据
    public void saveCartListByKey(String key, List<Cart> cartList) {
        redisTemplate.boundValueOps(key).set(cartList, 7L, TimeUnit.DAYS);
    }


    /**
     * 返回一个合并列表，前半段为redis中查询的数据，后半段为数据库查询的数据
     * @param key
     * @return
     */
    @Override
    //从redis中根据key获取购物车列表信息
    public List<Cart> selectCartListByKey(String key) {
        //从数据库获取购物车中的最新商品信息

        List<Cart> cartList = (List<Cart>) redisTemplate.boundValueOps(key).get();
        List<Cart> newCartList = new ArrayList<>();//最新商品信息
        //遍历redis购物车列表数据，查询数据库中最新的购物车商品
        if (cartList!=null){//如果查到的购物车列表【没有失效】
            for (int i = 0; i < cartList.size(); i++) {
                Cart newCart = new Cart();
                newCart.setSellerId(cartList.get(i).getSellerId());
                newCart.setSellerName(cartList.get(i).getSellerName());
                newCart.setOrderItemList(new ArrayList<>());
                newCartList.add(newCart);
                for (int j = 0; j < cartList.get(i).getOrderItemList().size(); j++) {
                    //根据redis中存的购物车数据 查询数据库中的数据
                    TbItem item =
                            itemMapper.selectByPrimaryKey(cartList.get(i).getOrderItemList().get(j).getItemId());
                    //根据查询到的item数据 组装orderItem，并存入最新的购物车列表中
                    newCartList.get(i).getOrderItemList().add(
                            createOrderItem(item,cartList.get(i).getOrderItemList().get(j).getNum())
                    );
                }
                if(newCartList.get(i).getOrderItemList().size()==0){
                    newCartList.remove(i);
                }
            }
            /**
             * 这里从数据库查找到数据后不刷新redis【因为需要统计从加入购物车到本次查看共变化多少】
             */
//            if (redisTemplate.getExpire(key)>0){//如果存在过期时间，则为游客登录
//                saveCartListByKey(key,newCartList);//将购物车数据更新到最新，并设置生存时间7天
//            }else{//否则 用户为username登录，不设置生存时间
//                saveCartListByUsername(key, newCartList);
//            }
            cartList.addAll(newCartList);//将【最新商品数据】列表添加到购物车列表的【后半段】
            return cartList;
        }

        return new ArrayList<>();
    }

    @Override
    //当购物车列表形成订单后 删除基于username保存的购物车列表
    public void saveCartListByUsername(String username, List<Cart> cartList) {
        redisTemplate.boundValueOps(username).set(cartList);
    }

    @Override
    //合并的结果存入到redis中
    /**
     * （由于查询到的列表数据是正常数据的2倍：前半段为redis中购物车数据，后半段为数据库中最新购物车数据
     *      所以合并时 两列表前半段合并，两后半段合并，最后两结果列表相加即可）
      */
    public List<Cart> mergeCartList(List<Cart> cartList_sessionId, List<Cart> cartList_username) {
        List<Cart> cartList_sessionId_pre = cartList_sessionId.subList(0, cartList_sessionId.size() / 2);//截取前一半（session Redis）
        List<Cart> cartList_username_pre = cartList_username.subList(0, cartList_username.size() / 2);//截取前一半（username Redis）
        List<Cart> cartList_sessionId_suf = new ArrayList<>();
        List<Cart> cartList_username_suf = new ArrayList<>();
        cartList_sessionId_suf.addAll(cartList_sessionId.subList(cartList_sessionId.size() / 2, cartList_sessionId.size()));//截取后一半（session mysql）
        cartList_username_suf.addAll(cartList_username.subList(cartList_username.size() / 2, cartList_username.size()));//截取后一半（username mysql）

        //合并sessionId与username的购物车列表【前半段】
        mergeSubCartList(cartList_sessionId_pre,cartList_username_pre);
        //合并sessionId与username的购物车列表【后半段】
        mergeSubCartList(cartList_sessionId_suf,cartList_username_suf);
        cartList_username_pre.addAll(cartList_username_suf);//相加合并后的前后两段：[【redis数据】,【数据库数据】]
        return cartList_username_pre;
    }

    private void mergeSubCartList(List<Cart> cartList_sessionId, List<Cart> cartList_username) {
        for (Cart cart_sessionId : cartList_sessionId) {
            String sellerId = cart_sessionId.getSellerId();
            Cart cart = searchCartBySellerId(cartList_username, sellerId);//查询基于username的购物车是否有该商家
            if (cart==null){//如果没有该商家
                cart = new Cart();
                cart.setSellerId(sellerId);
                cart.setSellerName(cart_sessionId.getSellerName());
                cart.setOrderItemList(cart_sessionId.getOrderItemList());
                cartList_username.add(cart);//加入username购物车列表
            }else {//如果存在商家
                //查询session列表中的商家下的商品id是否也存在于username列表中
                List<TbOrderItem> orderItemList = cart.getOrderItemList();
                for (TbOrderItem orderItem_sessionId_pre : cart_sessionId.getOrderItemList()) {
                    //如果session列表中的商家下的商品id也存在于username列表中 则返回该商品项
                    TbOrderItem orderItem = searchOrderItemByItemId(orderItemList, orderItem_sessionId_pre.getItemId());
                    if (orderItem!=null){  //如果存在于username的列表中
                        orderItem.setNum(orderItem.getNum()+orderItem_sessionId_pre.getNum());//合并数量
                        orderItem.setTotalFee(orderItem.getTotalFee().add(orderItem_sessionId_pre.getTotalFee()));//合并totalFee
                    }else {//如果session列表中该商家下商品不存在于 username列表中
                        orderItemList.add(orderItem_sessionId_pre);//将session列表中商品添加到username中列表
                    }
                }
            }
        }
    }

    @Override
    //清除登陆前的购物车列表
    public void deleCartListBySessionId(String sessionId) {
        redisTemplate.delete(sessionId);
    }

    /**
     * 根据sku的id（itemId）查询所有该spu的所有sku
     * @param itemId
     * @return
     */
    @Override
    public List<TbItem> selectTitleByItemId(Long itemId) {
        //根据sku的id查询该商品
        TbItem item = itemMapper.selectByPrimaryKey(itemId);
        //根据商品信息获取spu的id
        Long goodsId = item.getGoodsId();
        TbItemExample example = new TbItemExample();
        TbItemExample.Criteria criteria = example.createCriteria();
        criteria.andGoodsIdEqualTo(goodsId);//封装goodsId
        List<TbItem> itemList = itemMapper.selectByExample(example);
        if (itemList!=null){
            return itemList;
        }
        return new ArrayList<>();
    }

    /**
     * 改变购物车中商品的规格，将oldItemId的商品更换为newItemId的商品
     * @return
     */
    @Override
    public List<Cart> changeCartList(List<Cart> cartList,Long newItemId, Long oldItemId) {
        Integer [] newItemIndex = getOrderItemIndexByItemId(cartList,newItemId);//根据itemId获取商品在购物车中的位置
        Integer [] oldItemIndex = getOrderItemIndexByItemId(cartList,oldItemId);

        if (oldItemIndex==null){
            throw new RuntimeException("商品itemId参数错误！");
        }
        TbOrderItem orderItem = cartList.get(oldItemIndex[0]).getOrderItemList().get(oldItemIndex[1]);
        //从数据库中查询商品数据
        TbItem item = itemMapper.selectByPrimaryKey(newItemId);
        TbOrderItem newOrderItem = createOrderItem(item, orderItem.getNum());
        //添加（替换）至购物车中
        cartList.get(oldItemIndex[0]).getOrderItemList().set(oldItemIndex[1], newOrderItem);
        if (newItemIndex!=null){//如果新添加进来的商品在原有购物车里也存在
            //合并两个相同itemId的商品（将旧的购物车信息(newIndex)的数量合并到新(oldIndex)的中，并删除旧(newIndex)信息）
            Integer num = cartList.get(newItemIndex[0]).getOrderItemList().get(newItemIndex[1]).getNum();
            orderItem = cartList.get(oldItemIndex[0]).getOrderItemList().get(oldItemIndex[1]);
            orderItem.setNum(orderItem.getNum()+num);//设置总商品数量
            //设置总金额
            orderItem.setTotalFee(new BigDecimal(orderItem.getNum() * orderItem.getPrice().doubleValue()));
            //删除旧(newIndex)信息
            cartList.get(newItemIndex[0]).getOrderItemList().remove(cartList.get(newItemIndex[0]).getOrderItemList().get(newItemIndex[1]));

        }
        return cartList;
    }

    /**
     * 根据itemId获取商品在购物车中的位置
     * @param itemId
     * @return
     */
    private Integer[] getOrderItemIndexByItemId(List<Cart> cartList,Long itemId) {
        for (int i = 0; i < cartList.size(); i++) {
            List<TbOrderItem> orderItemList = cartList.get(i).getOrderItemList();
            for (int j = 0; j < orderItemList.size(); j++) {
                TbOrderItem orderItem = orderItemList.get(j);
                if (orderItem.getItemId().equals(itemId)){//如果找到itemId，则返回位置信息
                    return new Integer[]{i,j};
                }
            }
        }
        return null;//如果没有找到，则返回null
    }

    private TbOrderItem searchOrderItemByItemId(List<TbOrderItem> orderItemList, Long id) {
        for (TbOrderItem orderItem : orderItemList) {
            if (orderItem.getItemId().longValue()==id){//如果找到该商品id 返回该商品
                return orderItem;
            }
        }
        return null;//如果没有找到 返回空
    }

    private TbOrderItem createOrderItem(TbItem item, Integer num) {
        if (num<1){
            throw new RuntimeException("添加的商品数量不能小于1！");
        }
        TbOrderItem orderItem = new TbOrderItem();
        //设置购物车中商品的详细信息
        orderItem.setItemId(item.getId());
        orderItem.setGoodsId(item.getGoodsId());
        orderItem.setTitle(item.getTitle());
        orderItem.setPrice(item.getPrice());
        orderItem.setNum(num);
        orderItem.setTotalFee(new BigDecimal(orderItem.getPrice().doubleValue()*orderItem.getNum()));
        orderItem.setPicPath(item.getImage());
        orderItem.setSellerId(item.getSellerId());
        return orderItem;
    }


    //遍历购物车列表，判断商家是否已经存在,若存在，则返回该商家
    private Cart searchCartBySellerId(List<Cart> cartList, String sellerId) {
        for (Cart cart : cartList) {
            if (sellerId.equals(cart.getSellerId())){//如果找到同一个商家
                return cart;
            }
        }
        return null;//如果没有找到商家，则返null
    }
}
