package com.pinyougou.pay.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.wxpay.sdk.WXPayUtil;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.mapper.TbPayLogMapper;
import com.pinyougou.pay.service.PayService;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbPayLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import util.HttpClient;

import java.util.*;

@Service
@Transactional
public class PayServiceImpl implements PayService {

    @Value("${appid}")
    private String appid;//公众号id

    @Value("${partner}")
    private String partner;//商户号id

    @Value("${partnerkey}")
    private String partnerkey;//商户秘钥

    @Value("${notifyurl}")
    private String notifyurl;//回调地址

    /**
     * 【生成二维码】
     *
     * @out_trade_no : 订单号
     * @total_fee : 支付金额（单位：分）
     */
    @Override
    public Map<String, Object> createNative(String out_trade_no, String total_fee) throws Exception {
        //1.组装调用统一下单接口所必须的参数
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("appid", appid);  //设置公众号id
        paramMap.put("mch_id", partner);//设置商户号id
        paramMap.put("nonce_str", WXPayUtil.generateNonceStr());//设置随机字符串
        paramMap.put("body", "蔡虚鲲的爱心小屋");//支付时提示信息
        paramMap.put("out_trade_no", out_trade_no);//支付订单号码
        paramMap.put("total_fee", total_fee);//设置支付金额
        paramMap.put("spbill_create_ip", "127.0.0.1");//设置终端ip
        paramMap.put("notify_url", notifyurl);
        paramMap.put("trade_type", "NATIVE");//设置支付方式为扫码支付
        paramMap.put("product_id", "1");//商品id（随意填的）

        //2.调用微信支付统一下单接口 获取支付的超链接
        String paramXml = WXPayUtil.generateSignedXml(paramMap, partnerkey);
        System.out.println(paramXml);
        //发请求
        HttpClient httpClient = new HttpClient("https://api.mch.weixin.qq.com//pay/unifiedorder");
        httpClient.setHttps(true);
        httpClient.setXmlParam(paramXml);
        httpClient.post();//组装参数完之后 发送post请求

        //3.处理支付平台返回的响应结果
        String resultXml = httpClient.getContent();
        Map<String, String> resultMap = WXPayUtil.xmlToMap(resultXml);
        String code_url = resultMap.get("code_url");

        //构建返回参数集合
        Map<String, Object> map = new HashMap<>();
        map.put("out_trade_no", out_trade_no);//订单号
        map.put("total_fee", total_fee);//支付金额
        map.put("code_url", code_url);//支付链接
        return map;
    }

    @Override
    public Map<String, String> queryPayStatus(String out_trade_no) throws Exception {
        //1.封装必须的参数
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("appid", appid); //公众号id
        paramMap.put("mch_id", partner); //商户id
        paramMap.put("nonce_str", WXPayUtil.generateNonceStr());//随机字符串
        paramMap.put("out_trade_no", out_trade_no);
        //2.调用微信的查询支付状态的接口
        String paramXml = WXPayUtil.generateSignedXml(paramMap, partnerkey);
        HttpClient httpClient = new HttpClient("https://api.mch.weixin.qq.com//pay/orderquery");
        httpClient.setXmlParam(paramXml);
        httpClient.setHttps(true);
        httpClient.post();//发送查询请求

        String resultXml = httpClient.getContent(); //返回xml字符串
        Map<String, String> resultMap = WXPayUtil.xmlToMap(resultXml);//用微信的util解析xml成map
        return resultMap;
    }

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbPayLogMapper payLogMapper;

    @Autowired
    private TbOrderMapper orderMapper;

    @Override
    //根据userId获取登陆人的订单日志信息
    public TbPayLog getPayLog(String userId) {
        return (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
    }

    @Override
    public void updatePayStatus(String out_trade_no, String transaction_id) {
        //支付成功后，更新订单状态和支付日志状态
        TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
        payLog.setPayTime(new Date());//设置支付时间
        payLog.setTransactionId(transaction_id);//设置微信流水号【方便查询】
        payLog.setTradeState("2");//设置支付状态为【已支付】
        payLogMapper.updateByPrimaryKey(payLog);//更新支付日志的微信流水号状态

        //更新订单状态
        String orderList = payLog.getOrderList();//获取订单id列表
        String[] orderIds = orderList.split(",");
        for (String orderId : orderIds) {//遍历订单id，更新支付状态
            TbOrder tbOrder = orderMapper.selectByPrimaryKey(Long.parseLong(orderId));
            tbOrder.setPaymentTime(new Date());//设置支付时间
            tbOrder.setStatus("2");//设置支付状态为【已支付】
            orderMapper.updateByPrimaryKey(tbOrder);//更新数据库 订单的信息
        }
        //支付成功后 清除redis中的支付日志信息
        redisTemplate.boundHashOps("payLog").delete(payLog.getUserId());
    }

    /**
     * 判断总时间是否在规定时间之内(5分钟)
     * @param out_trade_no
     * @param time
     * @return
     */
    @Override
    public void judgeTimeOut(String userId,String out_trade_no, long time) {
        TbPayLog payLog = (TbPayLog) redisTemplate.boundHashOps("payLog").get(userId);
        if (payLog!=null){
            String outTradeNo = payLog.getOutTradeNo();
            if (outTradeNo.equals(out_trade_no)){//如果前端页面传过来的订单日志id等于redis中查询到的id，则继续
                Date createTime = payLog.getCreateTime();
                String status = payLog.getTradeState();
                if (!"0".equals(status) && new Date().getTime()-createTime.getTime()>time){//如果订单没有删除，而且订单时间超时
                    payLog.setTradeState("0");//删除订单
                    payLogMapper.updateByPrimaryKey(payLog);//保存至数据库
                    redisTemplate.boundHashOps("payLog").delete(userId);//删除redis中日志信息
                    throw new RuntimeException("payTimeOut");//订单超时标记
                }else if ("0".equals(status)) {//如果订单删除
                    throw new RuntimeException("信息异常，该订单已经删除！");//订单删除标记
                }else if ("1".equals(status) && new Date().getTime()-createTime.getTime()<time){
                    //如果状态为 未支付 并且没有超时
                    return;//订单处于等待支付状态，不抛异常
                }
            }
        }
        throw new RuntimeException("参数信息异常，请正常操作！");
    }

    @Override
    public Long getPayLogMoneyByOutTradeNo(String out_trade_no) {
        TbPayLog payLog = payLogMapper.selectByPrimaryKey(out_trade_no);
        if (payLog!=null){//如果存在该信息则返回真实支付的金额
            return payLog.getTotalFee();
        }else {
            return -1L;//否则返回-1
        }
    }


}
