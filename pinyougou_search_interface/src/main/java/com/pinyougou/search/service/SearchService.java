package com.pinyougou.search.service;

import java.util.Map;

public interface SearchService {

    /**
     * 商品搜索
     *  {keywords::"华为",brand:"",price:""}
     */
    public Map<String,Object> search(Map searchMap);

}
