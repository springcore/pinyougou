package cn.itcast.solr;

import com.pinyougou.pojo.TbItem;
import com.pinyougou.solr.util.SolrUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.Query;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.data.solr.core.query.SolrDataQuery;
import org.springframework.data.solr.core.query.result.ScoredPage;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/applicationContext*.xml")
public class SolrTest {

    @Autowired
    private SolrTemplate solrTemplate;

    @Autowired
    private SolrUtil solrUtil;
    @Test
    public void fun1(){
        System.out.println("11");
    }
/*
    *//**
     * 数据导入
     *//*
    @Test
    public void dataImport(){
        solrUtil.dataImport();
    }

    *//**
     * 新增（修改）数据
     *//*
    @Test
    public void save(){
        TbItem item = new TbItem();
        item.setId(2L);
        item.setBrand("华为");
        item.setTitle("华为p20 移动3G 64G");
        item.setSeller("华为官方旗舰店");
        solrTemplate.saveBean(item);
        solrTemplate.commit();//记得提交数据
    }

    *//**
     * 基于id查询
     *//*
    @Test
    public void getById(){
        TbItem item = solrTemplate.getById(1L, TbItem.class);
        System.out.println(item.getId()+"   "+item.getBrand()+"  "+item.getTitle()+"   "+item.getSeller());
    }

    *//**
     * 删除操作
     *//*
    @Test
    public void deleteById(){
        solrTemplate.deleteById("1");
        solrTemplate.commit();
    }

    *//**
     * 删除所有数据
     *//*
    @Test
    public void deleteAll(){
        SolrDataQuery query = new SimpleQuery("*:*");
        solrTemplate.delete(query);
        solrTemplate.commit();
    }

    *//**
     * 批量保存数据
     *//*
    @Test
    public void saveBatch(){
        List<TbItem> itemList = new ArrayList<>();
        for(long i=1;i<=100;i++){
            TbItem item = new TbItem();
            item.setId(i);
            item.setBrand("华为");
            item.setTitle(i+"华为p20 移动3G 64G");
            item.setSeller("华为"+i+"官方旗舰店");
            itemList.add(item);
        }
        solrTemplate.saveBeans(itemList);
        solrTemplate.commit();//记得提交数据
    }

    *//**
     * 分页查询
     *//*
    @Test
    public void queryPage(){
        Query query = new SimpleQuery("*:*");

        //设置分页查询条件
        query.setOffset(4);//指定查询分页数据起始值 默认是从0开始
        query.setRows(3);//每页查询记录数  默认是查询10条记录

        //参数一：查询对象
        ScoredPage<TbItem> page = solrTemplate.queryForPage(query, TbItem.class);

        System.out.println("总页数："+page.getTotalPages());
        System.out.println("总记录数："+page.getTotalElements());

        //获取当前页结果集列表数据
        List<TbItem> content = page.getContent();
        for (TbItem item : content) {
            System.out.println(item.getId()+"   "+item.getBrand()+"  "+item.getTitle()+"   "+item.getSeller());
        }
    }


    *//**
     * 条件查询
     *  商家中包含6，title中包含8
     *//*
    @Test
    public void multiQuery(){

        Query query = new SimpleQuery();

        //设置条件：商家中包含6，title中包含8
        Criteria criteria = new Criteria("item_seller").contains("6").and("item_title").contains("8");

        //关联设置好的条件对象到查询对象上
        query.addCriteria(criteria);

        //参数一：查询对象
        ScoredPage<TbItem> page = solrTemplate.queryForPage(query, TbItem.class);

        System.out.println("总页数："+page.getTotalPages());
        System.out.println("总记录数："+page.getTotalElements());

        //获取当前页结果集列表数据
        List<TbItem> content = page.getContent();
        for (TbItem item : content) {
            System.out.println(item.getId()+"   "+item.getBrand()+"  "+item.getTitle()+"   "+item.getSeller());
        }
    }*/



}
