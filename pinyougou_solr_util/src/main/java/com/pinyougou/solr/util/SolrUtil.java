package com.pinyougou.solr.util;

import com.alibaba.fastjson.JSON;
import com.pinyougou.mapper.TbItemMapper;
import com.pinyougou.pojo.TbItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class SolrUtil {

    @Autowired
    private TbItemMapper itemMapper;

    @Autowired
    private SolrTemplate solrTemplate;

    /**
     * 导入数据库满足条件的商品数据到索引库
     *  1、上架商品导入索引库  tb_goods  is_marketable='1'
        2、商品状态为1，正常状态 tb_item   status='1'
     */

    public void dataImport(){
       // 1、上架商品导入索引库  tb_goods  is_marketable='1' 2、商品状态为1，正常状态 tb_item   status='1'
        List<TbItem> itemList = itemMapper.findAllGrounding();

        for (TbItem item : itemList) {
            //为索引库动态域赋值  {"网络":"移动4G","机身内存":"32G"}
            String spec = item.getSpec();
            Map<String,String> specMap = JSON.parseObject(spec, Map.class);
            item.setSpecMap(specMap);
        }

        //导入数据库
        solrTemplate.saveBeans(itemList);
        solrTemplate.commit();

    }

}
