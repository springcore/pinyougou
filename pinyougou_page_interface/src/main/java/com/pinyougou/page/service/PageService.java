package com.pinyougou.page.service;

import groupEntity.Goods;

public interface PageService {

    /**
     * 组装生成静态页所需要的参数
     */
    public Goods findOne(Long goodsId);
}
