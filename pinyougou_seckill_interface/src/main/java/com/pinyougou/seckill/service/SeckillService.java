package com.pinyougou.seckill.service;

import com.pinyougou.pojo.TbSeckillGoods;

import java.util.List;

public interface SeckillService {
    /**
     * 查询秒杀商品列表
     */
    public List<TbSeckillGoods> selectSeckillGoodsFromRedis();

    /**
     * 基于秒杀商品id查询秒杀商品详情
     */
    public TbSeckillGoods findOne(Long seckillGoodsId);

    void submitSeckillOrder(Long seckillGoodsId, String userId);
}
