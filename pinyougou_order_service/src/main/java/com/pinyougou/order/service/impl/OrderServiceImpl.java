package com.pinyougou.order.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pinyougou.cart.service.CartService;
import com.pinyougou.mapper.TbOrderItemMapper;
import com.pinyougou.mapper.TbPayLogMapper;
import com.pinyougou.order.service.OrderService;
import com.pinyougou.pojo.TbOrderItem;
import com.pinyougou.pojo.TbPayLog;
import groupEntity.Cart;
import org.springframework.beans.factory.annotation.Autowired;
import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.pinyougou.mapper.TbOrderMapper;
import com.pinyougou.pojo.TbOrder;
import com.pinyougou.pojo.TbOrderExample;
import com.pinyougou.pojo.TbOrderExample.Criteria;

import entity.PageResult;
import org.springframework.data.redis.core.RedisTemplate;
import util.IdWorker;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 服务实现层
 *
 * @author Administrator
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private TbOrderMapper orderMapper;

    @Autowired
    private IdWorker idWorker;

    /**
     * 查询全部
     */
    @Override
    public List<TbOrder> findAll() {
        return orderMapper.selectByExample(null);
    }

    /**
     * 按分页查询
     */
    @Override
    public PageResult findPage(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(null);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private TbOrderItemMapper orderItemMapper;

    @Autowired
    private TbPayLogMapper payLogMapper;

    @Reference
    private CartService cartService;

    /**
     * 增加
     */
    @Override
    public void add(TbOrder order, Long[] selectIds) {
        String userId = order.getUserId();
        //从购物车中获取信息 勾选的购物车选项中有几个商家 生成几条订单
        // （查询出的购物车列表为两部分：list=[【redis数据】,【数据库数据】]）
        List<Cart> cartList = cartService.selectCartListByKey(userId);
        //取后半段的【数据库数据】
        cartList = cartList.subList(cartList.size()/2, cartList.size());
        //新建一个列表 记录选择了的商品
        //根据选择了的id数组和购物车列表 生成已经选择的购物车列表
        List<Cart> selectCartList = buildSelectCartList(cartList, selectIds);

        //定义记录订单id的集合
        StringBuilder orderIds =new StringBuilder();

        double totalPayment = 0.00;
        //循环遍历选择了的商品
        for (Cart cart : selectCartList) {
            //生成订单
            TbOrder tbOrder = new TbOrder();
            long orderId = idWorker.nextId();
            orderIds.append(orderId).append(",");
            //设置订单id
            tbOrder.setOrderId(orderId);
            tbOrder.setStatus("1");//新生成的订单都是未支付状态(1:未支付，2:已支付)
            tbOrder.setCreateTime(new Date());
            tbOrder.setUpdateTime(new Date());
            tbOrder.setUserId(userId);
            tbOrder.setSourceType("2");//设置订单来源（1：app端，2：pc端，3：M端，4：微信端，5：手机app端）
            tbOrder.setSellerId(cart.getSellerId());

            /**
             * 组装页面提交的数据“
             */
            tbOrder.setPaymentType(order.getPaymentType());//设置支付方式
            tbOrder.setReceiverAreaName(order.getReceiverAreaName());//设置收货人收货地址名称
            tbOrder.setReceiverMobile(order.getReceiverMobile());//设置收货人手机号
            tbOrder.setReceiver(order.getReceiver());//设置收货人姓名


            double payment = 0.00;//用来累加SKU的小计金额
            //该订单购买的商品列表
            List<TbOrderItem> orderItemList = cart.getOrderItemList();
            for (TbOrderItem orderItem : orderItemList) {
                //设置主键id
                orderItem.setId(idWorker.nextId());
                //设置订单id
                orderItem.setOrderId(tbOrder.getOrderId());
                //累加订单项的小计金额
                payment += orderItem.getTotalFee().doubleValue();
                //保存商家下的订单项
                orderItemMapper.insert(orderItem);
            }
            //TODO 订单支付金额，将该商家下所有选择的商品小计相加即可（已完成✔）
            tbOrder.setPayment(new BigDecimal(payment));
            totalPayment += payment;
            orderMapper.insert(tbOrder);
        }
        orderIds.deleteCharAt(orderIds.lastIndexOf(","));//去掉最后一个逗号


        //如果支付方式采用在线支付 ，需要在系统中记录一笔支付操作，并且设支付状态为【未支付】
        if ("1".equals(order.getPaymentType())){
            TbPayLog payLog = new TbPayLog();
            payLog.setOutTradeNo(idWorker.nextId()+"");//支付订单号
            payLog.setCreateTime(new Date());//设置创建日期
            payLog.setTotalFee((long) (totalPayment*100));//设置支付金额
            payLog.setUserId(userId);//设置用户id
            payLog.setTradeState("1");//设置支付状态 1：未支付""
            payLog.setOrderList(orderIds.toString());//设置订单id列表（String类型:"1,2,3,4...."）
            payLog.setPayType("1");//设置支付方式 1：微信支付
            payLogMapper.insert(payLog);//将组装好的订单日志信息 存入数据库中
            //将订单日志信息信息存入缓存中，方便支付后返回页面信息
            redisTemplate.boundHashOps("payLog").put(userId, payLog);
        }

        //TODO 订单支付后，根据selectIds清除redis中已选择的购物车数据（未完成❌）
        //redisTemplate.delete(userId);//为了方便测试 懒得加购物车 直接不清除
        //for (Long selectId : selectIds) {
            //request.setAttribute("itemId", selectId);
            //request.setAttribute("num", -99999);
            //request.getRequestDispatcher("cart/addItemToCartList").forward(request, response);
            //CartService.addItemToCartList(selectId,-99999);//(这是个辣鸡办法,麻烦点的办法是，再在CartService写一个根据itemId数组删除redis购物车中商品的方法)
        //}

    }
//    @Autowired
//    private HttpServletRequest request;

//    @Autowired
//    private HttpServletResponse response;

    //根据选择了的id数组和购物车列表 生成已经选择的购物车列表
    private List<Cart> buildSelectCartList(List<Cart> cartList, Long[] selectIds) {
        List<Cart> selectCartList = new ArrayList<>();

        for (int i = 0; i < cartList.size(); i++) {
            Cart cart1 = new Cart();
            cart1.setOrderItemList(new ArrayList<>());
            selectCartList.add(cart1);
            Cart cart = cartList.get(i);
            selectCartList.get(selectCartList.size() - 1).setSellerId(cart.getSellerId());
            selectCartList.get(selectCartList.size() - 1).setSellerName(cart.getSellerName());
            for (int j = 0; j < cart.getOrderItemList().size(); j++) {
                TbOrderItem orderItem = cart.getOrderItemList().get(j);
                for (int k = 0; k < selectIds.length; k++) {
                    if (orderItem.getItemId().longValue() == selectIds[k]) {
                        selectCartList.get(selectCartList.size() - 1).getOrderItemList().add(orderItem);
                        break;
                    }
                }
            }
            if (selectCartList.get(selectCartList.size() - 1).getOrderItemList().size() == 0) {
                selectCartList.remove(selectCartList.size() - 1);
            }
        }


        return selectCartList;
    }


    /**
     * 修改
     */
    @Override
    public void update(TbOrder order) {
        orderMapper.updateByPrimaryKey(order);
    }

    /**
     * 根据ID获取实体
     *
     * @param id
     * @return
     */
    @Override
    public TbOrder findOne(Long id) {
        return orderMapper.selectByPrimaryKey(id);
    }

    /**
     * 批量删除
     */
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            orderMapper.deleteByPrimaryKey(id);
        }
    }


    @Override
    public PageResult findPage(TbOrder order, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);

        TbOrderExample example = new TbOrderExample();
        Criteria criteria = example.createCriteria();

        if (order != null) {
            if (order.getPaymentType() != null && order.getPaymentType().length() > 0) {
                criteria.andPaymentTypeLike("%" + order.getPaymentType() + "%");
            }
            if (order.getPostFee() != null && order.getPostFee().length() > 0) {
                criteria.andPostFeeLike("%" + order.getPostFee() + "%");
            }
            if (order.getStatus() != null && order.getStatus().length() > 0) {
                criteria.andStatusLike("%" + order.getStatus() + "%");
            }
            if (order.getShippingName() != null && order.getShippingName().length() > 0) {
                criteria.andShippingNameLike("%" + order.getShippingName() + "%");
            }
            if (order.getShippingCode() != null && order.getShippingCode().length() > 0) {
                criteria.andShippingCodeLike("%" + order.getShippingCode() + "%");
            }
            if (order.getUserId() != null && order.getUserId().length() > 0) {
                criteria.andUserIdLike("%" + order.getUserId() + "%");
            }
            if (order.getBuyerMessage() != null && order.getBuyerMessage().length() > 0) {
                criteria.andBuyerMessageLike("%" + order.getBuyerMessage() + "%");
            }
            if (order.getBuyerNick() != null && order.getBuyerNick().length() > 0) {
                criteria.andBuyerNickLike("%" + order.getBuyerNick() + "%");
            }
            if (order.getBuyerRate() != null && order.getBuyerRate().length() > 0) {
                criteria.andBuyerRateLike("%" + order.getBuyerRate() + "%");
            }
            if (order.getReceiverAreaName() != null && order.getReceiverAreaName().length() > 0) {
                criteria.andReceiverAreaNameLike("%" + order.getReceiverAreaName() + "%");
            }
            if (order.getReceiverMobile() != null && order.getReceiverMobile().length() > 0) {
                criteria.andReceiverMobileLike("%" + order.getReceiverMobile() + "%");
            }
            if (order.getReceiverZipCode() != null && order.getReceiverZipCode().length() > 0) {
                criteria.andReceiverZipCodeLike("%" + order.getReceiverZipCode() + "%");
            }
            if (order.getReceiver() != null && order.getReceiver().length() > 0) {
                criteria.andReceiverLike("%" + order.getReceiver() + "%");
            }
            if (order.getInvoiceType() != null && order.getInvoiceType().length() > 0) {
                criteria.andInvoiceTypeLike("%" + order.getInvoiceType() + "%");
            }
            if (order.getSourceType() != null && order.getSourceType().length() > 0) {
                criteria.andSourceTypeLike("%" + order.getSourceType() + "%");
            }
            if (order.getSellerId() != null && order.getSellerId().length() > 0) {
                criteria.andSellerIdLike("%" + order.getSellerId() + "%");
            }

        }

        Page<TbOrder> page = (Page<TbOrder>) orderMapper.selectByExample(example);
        return new PageResult(page.getTotal(), page.getResult());
    }

}
