 //控制层 
app.controller('seckillController' ,function($scope,$controller  ,$location ,$interval,seckillService){
	
	$controller('baseController',{$scope:$scope});//继承

    //查询秒杀商品列表
    $scope.selectSeckillGoodsList=function () {
        seckillService.selectSeckillGoodsList().success(function (response) {
            $scope.seckillGoodsList=response;
        })
    }

    //基于秒杀商品id查询秒杀商品详情
    $scope.findOne=function () {
        //获取秒杀商品首页传递到秒杀详情页面的秒杀商品id
        $scope.seckillGoodsId = $location.search()["seckillGoodsId"];
        seckillService.findOne($scope.seckillGoodsId).success(function (response) {
            $scope.seckillGoods=response;

            /**
             * 距离结束时间 格式化  01天 12:35:55
             */
            /*var endTime = new Date($scope.seckillGoods.endTime).getTime();//结束时间时间戳
            var nowTime = new Date().getTime();//当前时间时间戳
            var secondes = Math.floor((endTime-nowTime)/1000);//距离结束时间还剩多少秒
            var days = Math.floor (secondes/60/60/24);
            var hours= Math.floor((secondes-days*24*60*60)/60/60);
            var minutes =  Math.floor((secondes-days*24*60*60-hours*60*60)/60);
            var second= secondes-days*24*60*60-hours*60*60-minutes*60;*/
            //计算出剩余秒杀时间
            var endTime = new Date($scope.seckillGoods.endTime).getTime();
            var nowTime = new Date().getTime();
            $scope.secondes =Math.floor( (endTime-nowTime)/1000 );

            var time =$interval(function () {
                if($scope.secondes>0){
                    //时间递减
                    $scope.secondes--;
                    //时间格式化
                    $scope.timeString=$scope.convertTimeString($scope.secondes);
                }else{
                    //结束时间递减
                    $interval.cancel(time);
                }
            },1000);
        })
    }

    $scope.convertTimeString=function (allseconds) {
        //计算天数
        var days = Math.floor(allseconds/(60*60*24));

        //小时
        var hours =Math.floor( (allseconds-(days*60*60*24))/(60*60) );

        //分钟
        var minutes = Math.floor( (allseconds-(days*60*60*24)-(hours*60*60))/60 );

        //秒
        var seconds = allseconds-(days*60*60*24)-(hours*60*60)-(minutes*60);

        //拼接时间
        var timString="";
        if(days>0){
            timString=days+"天:";
        }

        if(hours<10){
            hours="0"+hours;
        }
        if(minutes<10){
            minutes="0"+minutes;
        }
        if(seconds<10){
            seconds="0"+seconds;
        }
        return timString+=hours+":"+minutes+":"+seconds;
    }


    //demo演示 数字递减效果
   /* $scope.count=10;
    //参数一：$interval每个多长时间要处理的事情 参数二：间隔时间（单位:毫秒） 参数三：执行次数，参数三可以省略
    $interval(function () {
        $scope.count--;
    }, 1000);*/
	
    //秒杀下订单
    $scope.submitSeckillOrder=function () {
        seckillService.submitSeckillOrder($scope.seckillGoodsId).success(function (response) {
            if(response.success){
                //秒杀下单成功，跳转支付页面
                location.href="pay.html";
            }else{
                alert(response.message);
            }
        })
    }
});
